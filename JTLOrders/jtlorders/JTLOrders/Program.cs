﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using DN_Classes.AppStatus;
using DN_Classes.JTL_Error;

namespace JTLOrders
{
    class Program
    {
        static void Main(string[] args)
        {
            int total = 0;
            int updated = 0;
            int newItems = 0;
            ApplicationStatus s = new ApplicationStatus("JTLOrders TOOLS_JTLOrders_136.22");
            String query = "SELECT kBestellung ," +
                                "tRechnung_kRechnung ," +
                                " tBenutzer_kBenutzer," +
                                " tAdresse_kAdresse," +
                                " tText_kText," +
                                " tKunde_kKunde," +
                                " cBestellNr," +
                                " cType," +
                                " cAnmerkung," +
                                " dErstellt," +
                                " nZahlungsziel," +
                                " tVersandArt_kVersandArt," +
                                " fVersandBruttoPreis," +
                                " fRabatt," +
                                " kInetBestellung," +
                                " cVersandInfo ," +
                                " dVersandt," +
                                " cIdentCode," +
                                " cBeschreibung," +
                                " cInet," +
                                " dLieferdatum," +
                                " kBestellHinweis," +
                                " cErloeskonto," +
                                " cWaehrung," +
                                " fFaktor," +
                                " kShop," +
                                " kFirma ," +
                                " kLogistik," +
                                " nPlatform," +
                                " kSprache," +
                                " fGutschein," +
                                " dGedruckt," +
                                " dMailVersandt," +
                                " cInetBestellNr," +
                                " kZahlungsArt," +
                                " kLieferAdresse," +
                                " kRechnungsAdresse," +
                                " nIGL," +
                                " nUStFrei," +
                                " cStatus," +
                                " dVersandMail," +
                                " dZahlungsMail," +
                                " cUserName," +
                                " cVerwendungszweck," +
                                " fSkonto," +
                                " kColor," +
                                " nStorno," +
                                " cModulID," +
                                " nZahlungsTyp," +
                                " nHatUpload," +
                                " fZusatzGewicht," +
                                " nKomplettAusgeliefert," +
                                " dBezahlt," +
                                " kSplitBestellung FROM dbo.tbestellung WHERE dErstellt BETWEEN DATEADD(day, -30 ,GETDATE()) AND GETDATE() " +
                                " ORDER BY dErstellt DESC";
            SQLServer sqlserver = new SQLServer("localhost", "sa", "JTL2015!", "eazybusiness");
            JTLOrderDB db = new JTLOrderDB();

            try
            {
                s.Start();
                Console.WriteLine("***********************************************************");

                Console.WriteLine("\t1. Executing SQL Command from SQLServer");

                DataTable results = sqlserver.SelectCommand(query);
                total = results.Rows.Count;
                Console.WriteLine("\t2. Saving result to MongoDB Collection");
                int counter = 0;
                List<string> rows = new List<string>();
                foreach (DataRow result in results.Rows)
                {
                    rows.Add(result[0].ToString());
                }

                foreach (DataRow result in results.Rows)
                {
                    counter++;
                    JTLOrderEntity entity = new JTLOrderEntity(result);
                    bool temp = db.Save(entity);
                }
                Console.WriteLine("\tSleeping for 2 hours\n");
                s.Successful();
                s.Stop();
            }
            catch (Exception ex)
            {
                s.AddMessagLine(ex.StackTrace);

                JTLErrorLog log = new JTLErrorLog();
                log.AddError(ex.StackTrace, "JTLOrders");
            }
        }


    }
}
