﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTLOrders
{
    public class JTLOrderDB
    {

        
        public static MongoClient Client;
        public static MongoServer Server;
        public static MongoDatabase PDatabase;
        public static MongoCollection<JTLOrderEntity> mongoCollection;

      
        public JTLOrderDB() {
            Client = new MongoClient(@"mongodb://client148:client148devnetworks@136.243.44.111/JTLOrders");
            Server = Client.GetServer();
            PDatabase = Server.GetDatabase("JTLOrders");
            mongoCollection = PDatabase.GetCollection<JTLOrderEntity>("jtlorders");
        
        }

        public void RemoveAll() {
            mongoCollection.RemoveAll();
        }
        public bool Save(JTLOrderEntity entity) {
            JTLOrderEntity temp = mongoCollection.FindOne(Query.EQ("order.kBestellung", entity.order.kBestellung));
                if(temp==null){
                //    Console.WriteLine(entity.order.kBestellung+ " SAVED");
                       mongoCollection.Save(entity);
                       return true;
                }else{
                    temp.order= entity.order;
              //      Console.WriteLine(entity.order.kBestellung + " UPDATED");
            
                    mongoCollection.Save(temp);
                    return false;
                }


       }
    }
}
