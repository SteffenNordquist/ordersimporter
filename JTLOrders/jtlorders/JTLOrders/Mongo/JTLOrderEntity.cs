﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
namespace JTLOrders
{
    [BsonIgnoreExtraElements]
    public class JTLOrderEntity
    {
        public ObjectId id { get; set; }
        public Order order { get; set; }
        public BsonDocument plus { get; set; }


      
        public JTLOrderEntity(DataRow row)
        {
            order = new Order();

            order. kBestellung = row[0].ToString();
            order.tRechnung_kRechnung = row[1].ToString();
            order.tBenutzer_kBenutzer = Convert.ToInt64(row[2].ToString());
            order.tAdresse_kAdresse = row[3].ToString();
            order.tText_kText = Convert.ToInt64(row[4].ToString());
            order.tKunde_kKunde = Convert.ToInt64(row[5].ToString());
            order.cBestellNr = row[6].ToString();
            order.cType = row[7].ToString();
            order.cAnmerkung = row[8].ToString();
            try
            {
                order.dErstellt = Convert.ToDateTime(row[9]);
            }
            catch {
                Console.WriteLine(row[9].ToString()+"\t9");
            }

            order.nZahlungsziel = row[10].ToString();
            order.tVersandArt_kVersandArt = row[11].ToString();
            order.fVersandBruttoPreis = row[12].ToString();
            order.fRabatt = row[13].ToString();
            order.kInetBestellung = row[14].ToString();
            order.cVersandInfo = row[15].ToString();

            
            if (row["dVersandt"].ToString() == "") //dversadnt row
            {

                //check if leiferdatum is not empty, if its not, copy the value to dversandt
                // leiferdatum && nKomplettAusgeliefert && nStorno
                if (row["dLieferdatum"].ToString() != "" && Convert.ToInt32(row["nKomplettAusgeliefert"].ToString()) > 0 && Convert.ToInt32(row["nStorno"].ToString()) == 0) 
                {
                    order.dVersandt = row["dLieferdatum"].ToString();
                    SetShipDateTime(row["dLieferdatum"].ToString());
                }
                else {
                    order.dVersandt = row["dVersandt"].ToString();
                }
            }
            else {

                order.dVersandt = row["dVersandt"].ToString();
                SetShipDateTime(row["dVersandt"].ToString());
            }
           
           
            order.cIdentCode = row[17].ToString();
            order.cBeschreibung = row[18].ToString();
            order.cInet = row[19].ToString();
            order.dLieferdatum = row[20].ToString();
            order.kBestellHinweis = row[21].ToString();
            order.cErloeskonto = row[22].ToString();
            order.cWaehrung = row[23].ToString();
            order.fFaktor = row[24].ToString();
            order.kShop = row[25].ToString();
            order.kFirma = row[26].ToString();
            order.kLogistik = row[27].ToString();
            order.nPlatform = row[28].ToString();
            order.kSprache = row[29].ToString();
            order.fGutschein = row[30].ToString();
            order.dGedruckt = row[31].ToString();
            order.dMailVersandt = row[32].ToString();
            order.cInetBestellNr = row[33].ToString();
            order.kZahlungsArt = row[34].ToString();
            order.kLieferAdresse = row[35].ToString();
            order.kRechnungsAdresse = row[36].ToString();
            order.nIGL = row[37].ToString();
            order.nUStFrei = row[38].ToString();
            order.cStatus = row[39].ToString();
            order.dVersandMail = row[40].ToString();
            order.dZahlungsMail = row[41].ToString();
            order.cUserName = row[42].ToString();
            order.cVerwendungszweck = row[43].ToString();
            order.fSkonto = row[44].ToString();
            order.kColor = row[45].ToString();
            order.nStorno = row[46].ToString();
            order.cModulID = row[47].ToString();
            order.nZahlungsTyp = row[48].ToString();
            order.nHatUpload = row[49].ToString();
            order.fZusatzGewicht = row[50].ToString();
            order.nKomplettAusgeliefert = row[51].ToString();
            order.dBezahlt = row[52].ToString();
            order.kSplitBestellung = row[53].ToString();
            plus = new BsonDocument();
      }

        private void SetShipDateTime(string DateRow)
        {
            try
            {
                order.shipDateTime = Convert.ToDateTime(DateRow);
            }
            catch
            {

                order.shipDateTime = DateTime.MinValue;
            }
        }
    }

    [BsonIgnoreExtraElements]
    public class Order {
        public string kBestellung { get; set; }
        public string tRechnung_kRechnung { get; set; }
        public long tBenutzer_kBenutzer { get; set; }
        public string tAdresse_kAdresse { get; set; }
        public long tText_kText { get; set; }
        public long tKunde_kKunde { get; set; }
        public string cBestellNr { get; set; }
        public string cType { get; set; }
        public string cAnmerkung { get; set; }
        public DateTime dErstellt { get; set; }
        public string nZahlungsziel { get; set; }
        public string tVersandArt_kVersandArt { get; set; }
        public string fVersandBruttoPreis { get; set; }
        public string fRabatt { get; set; }
        public string kInetBestellung { get; set; }
        public string cVersandInfo { get; set; }
        public string dVersandt { get; set; }
        public string cIdentCode { get; set; }
        public string cBeschreibung { get; set; }
        public string cInet { get; set; }
        public string dLieferdatum { get; set; }
        public string kBestellHinweis { get; set; }
        public string cErloeskonto { get; set; }
        public string cWaehrung { get; set; }
        public string fFaktor { get; set; }
        public string kShop { get; set; }
        public string kFirma { get; set; }
        public string kLogistik { get; set; }
        public string nPlatform { get; set; }
        public string kSprache { get; set; }
        public string fGutschein { get; set; }
        public string dGedruckt { get; set; }
        public string dMailVersandt { get; set; }
        public string cInetBestellNr { get; set; }
        public string kZahlungsArt { get; set; }
        public string kLieferAdresse { get; set; }
        public string kRechnungsAdresse { get; set; }
        public string nIGL { get; set; }
        public string nUStFrei { get; set; }
        public string cStatus { get; set; }
        public string dVersandMail { get; set; }
        public string dZahlungsMail { get; set; }
        public string cUserName { get; set; }
        public string cVerwendungszweck { get; set; }
        public string fSkonto { get; set; }
        public string kColor { get; set; }
        public string nStorno { get; set; }
        public string cModulID { get; set; }
        public string nZahlungsTyp { get; set; }
        public string nHatUpload { get; set; }
        public string fZusatzGewicht { get; set; }
        public string nKomplettAusgeliefert { get; set; }
        public string dBezahlt { get; set; }
        public string kSplitBestellung { get; set; }
        public DateTime shipDateTime { get; set; }

    }
}
