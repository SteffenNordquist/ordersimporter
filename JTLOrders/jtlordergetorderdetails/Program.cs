﻿using DN_Classes.AppStatus;
using DN_Classes.Entities.Warehouse;
using DN_Classes.Queries;
using DN_Classes.QueriesCommands.SingleCalculatedOrders;
using DN_Classes.QueriesCommands.Supplier;
using DN_Classes.QueriesCommands.Warehouse_JTL_;
using JTLOrdersGetDetails.Classes;
using JTLOrdersGetDetails.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN_Classes.JTL_Error;

namespace JTLOrdersGetDetails
{
    class Program
    {
       

        static void Main(string[] args)
        {
            string appstatus = Properties.Settings.Default.AppStatusName;
            using (ApplicationStatus appStatus = new ApplicationStatus(appstatus))
            {
                List<IOrdersDB> orderdbs = new List<IOrdersDB>();
                orderdbs.Add(new AmazonDB());
                orderdbs.Add(new EbayDB());

                JTLOrders jtlorders = new JTLOrders();

                var jtlOrders = jtlorders.GetJTLOrders();

                Console.WriteLine("About to Update {0} orders", jtlOrders.Count());
                foreach (var order in jtlOrders) {

                    if (order.order.cInetBestellNr != "")
                    {
                        foreach (IOrdersDB orderdb in orderdbs)
                        {
                            try
                            {
                               
                                IOrders iorder = orderdb.GetOrderType(order.order.cInetBestellNr);
                                var property = iorder.GetType().GetProperty("orderDetails");
                                object orderDetails = property.GetValue(iorder, null);

                                if (iorder.GetType() == typeof(Amazon))
                                {
                                    jtlorders.UpdateJtlOrder(order.order.cInetBestellNr, (WareHouseOrdersEntity)orderDetails);
                                    Console.WriteLine("AmazonOrder updated - {0}", order.order.cInetBestellNr);
                                    break;
                                }

                                if (iorder.GetType() == typeof(Ebay))
                                {
                                    jtlorders.UpdateJtlOrder(order.order.cInetBestellNr, (WareHouseOrdersEntity_Ebay)orderDetails);
                                    Console.WriteLine("Ebay Order updated - {0}", order.order.cInetBestellNr);
                                }

                            }
                            catch(Exception ex) {
                                JTLErrorLog log=new JTLErrorLog();
                                log.AddError(ex.StackTrace, "JTLOrdersGetDetails");
                                appStatus.AddMessagLine("it breaks, please check");
                               
                              
                            }
                        }
                    }
                }

                appStatus.AddMessagLine(" not running");
                appStatus.Successful();

            }
           
        }

        
    }
}
