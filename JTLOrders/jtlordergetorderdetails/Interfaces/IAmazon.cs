﻿using DN_Classes.Entities.Warehouse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTLOrdersGetDetails.Interfaces
{
    public interface IAmazon 
    {
        
        DateTime GetLatestShipmentDate();
        string GetBuyerEmail();
        string GetBuyerName();
      
    }
}
