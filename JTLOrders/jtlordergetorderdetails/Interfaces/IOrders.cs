﻿using DN_Classes.Entities;
using DN_Classes.Entities.Warehouse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTLOrdersGetDetails.Interfaces
{
    public interface IOrders
    {
       
        
        string GetOrderId();
        DateTime GetPurchasedDate();
        string GetAgentName();
        string GetPlatform();
        List<WareHouseOrdersDetailsEntity> GetItems();
        bool GetReUpdateStatus();


    }
}
