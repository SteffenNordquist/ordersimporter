﻿using DN_Classes.Entities;
using DN_Classes.Entities.Warehouse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTLOrdersGetDetails.Interfaces
{
   public interface IJtlOrders
    {
       List<JTLOrderEntity> GetJTLOrders();
       void UpdateJtlOrder(string orderId, WareHouseOrdersEntity detail);
       void UpdateJtlOrder(string orderId, WareHouseOrdersEntity_Ebay detail);
    }
}
