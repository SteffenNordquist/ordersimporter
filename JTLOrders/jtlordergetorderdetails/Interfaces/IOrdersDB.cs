﻿using DN_Classes.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTLOrdersGetDetails.Interfaces
{
    public interface IOrdersDB
    {
        List<CalculatedProfitEntity> order { get;}
        List<CalculatedProfitEntity> GetProductByOrderItem(string orderId);
        IOrders GetOrderType(string orderId);
    }
}
