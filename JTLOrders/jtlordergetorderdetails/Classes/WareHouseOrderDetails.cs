﻿using DN_Classes.Entities;
using DN_Classes.Entities.Warehouse;
using JTLOrdersGetDetails.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTLOrdersGetDetails.Classes
{
    public class WareHouseOrderDetails : IItemDetails
    {

        private List<CalculatedProfitEntity> orderItems { get; set; }
        public List<WareHouseOrdersDetailsEntity> Items { get; set; }
        private CalculatedProfitEntity orderItem { get; set; }

        public WareHouseOrderDetails(List<CalculatedProfitEntity> orderItems) {
            List<WareHouseOrdersDetailsEntity> itemList = new List<WareHouseOrdersDetailsEntity>();
            foreach (var orderItem in orderItems) {

                this.orderItem = orderItem;
                WareHouseOrdersDetailsEntity wh = new WareHouseOrdersDetailsEntity()
                {
                        SupplierName = GetSupplierName(),
                        Ean  = GetEan(),
                        SellerSKU = GetSellerSKU(),
                        ArtNr =  GetArtNr(),
                        SupplierOrderId = GetSupplierOrderId()

                };
            
            itemList.Add(wh);
            
            }

            this.Items = itemList;
        }

        public string GetSupplierName()
        {
            return orderItem.SupplierName;
        }

        public string GetEan()
        {
            return orderItem.Ean;
        }

        public string GetSellerSKU()
        {
            return orderItem.SellerSku;
        }

        public string GetArtNr()
        {
            return orderItem.ArticleNumber;
        }

        public string GetSupplierOrderId()
        {
             

             if (GetSupplierName() == "JTL")
            {
                return "JTL";
            }
            else {

                if (orderItem.plus!= null && orderItem.plus.Contains("supplierOrderId"))
                {
                    return orderItem.plus["supplierOrderId"].ToString();
                }
                else {  return ""; }
                }
           
        
        }
    }
}
