﻿using DN_Classes.Entities;
using DN_Classes.Entities.Warehouse;
using JTLOrdersGetDetails.Classes;
using JTLOrdersGetDetails.Interfaces;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTLOrdersGetDetails
{
    public class Amazon : IAmazon, IOrders
    {
        private List<CalculatedProfitEntity> orderItems { get; set; }
        public WareHouseOrdersEntity orderDetails { get; set; }
        private List<WareHouseOrdersDetailsEntity> Items { get; set; }

        public Amazon(List<CalculatedProfitEntity> orderItems)
        {
            
                this.orderItems = orderItems;
                this.Items = GetItems();
                WareHouseOrdersEntity whOrdersDetails = new WareHouseOrdersEntity
                       {
                           AmazonOrderId = GetOrderId(),
                           PurchasedDate = GetPurchasedDate(),
                           Items = this.Items,
                           ReUpdate = GetReUpdateStatus(),
                           LatestShipmentDate = GetLatestShipmentDate(),
                           BuyerEmail = GetBuyerEmail(),
                           BuyerName = GetBuyerName(),
                           AgentName = GetAgentName(),
                           Platform = GetPlatform()
                       };

                this.orderDetails = whOrdersDetails;
              
        }



        public string GetOrderId()
        {
            return orderItems.First().AmazonOrder.AmazonOrderId;
        }

        public DateTime GetPurchasedDate()
        {
            return COnvertStringToDate(orderItems.First().AmazonOrder.PurchaseDate);
        }


        public DateTime GetLatestShipmentDate()
        {
            return COnvertStringToDate(orderItems.First().AmazonOrder.LatestShipDate);
        }

        public string GetBuyerEmail()
        {
            return orderItems.First().AmazonOrder.BuyerEmail;
        }

        public string GetBuyerName()
        {
            return orderItems.First().AmazonOrder.BuyerName;
        }

        public string GetAgentName()
        {
            return orderItems.First().Agentname;
        }

        public string GetSupplierName()
        {
            return orderItems.First().SupplierName;
        }

        public string GetPlatform()
        {
            return "Amazon";
        }

        private DateTime COnvertStringToDate(string shipmentDate)
        {
            DateTime newShipmentDate = DateTime.Now;
            if (shipmentDate.Contains("."))
            {
                newShipmentDate = DateTime.Parse(shipmentDate, new CultureInfo("de-DE"));
            }
            else
            {
                newShipmentDate = DateTime.ParseExact(shipmentDate, "M/d/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);

            }

            return newShipmentDate;
        }


        public List<WareHouseOrdersDetailsEntity> GetItems()
        {
            WareHouseOrderDetails details = new WareHouseOrderDetails(this.orderItems);

            return details.Items;
        }


        public bool GetReUpdateStatus()
        {
            int count = this.Items.Where(x => x.SupplierOrderId == "").Count();

            if( count > 0 ){ return true;}
            else { return false; }
        }
    }
}
