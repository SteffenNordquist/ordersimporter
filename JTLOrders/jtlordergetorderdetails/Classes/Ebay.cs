﻿using DN_Classes.Entities;
using DN_Classes.Entities.Warehouse;
using JTLOrdersGetDetails.Interfaces;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTLOrdersGetDetails.Classes
{
    public class Ebay : IEbay, IOrders
    {
        private List<CalculatedProfitEntity> orderItems { get; set; }
        public WareHouseOrdersEntity_Ebay orderDetails { get; set; }
        private List<WareHouseOrdersDetailsEntity> Items { get; set; }

        public Ebay(List<CalculatedProfitEntity> orderItems)
        {

            this.orderItems = orderItems;
            this.Items = GetItems();
            WareHouseOrdersEntity_Ebay whOrdersDetails = new WareHouseOrdersEntity_Ebay
                   {
                       OrderId = GetOrderId(),
                       PurchasedDate = GetPurchasedDate(),
                       Items = GetItems(),
                       AgentName = GetAgentName(),
                       Platform = GetPlatform(),
                       ReUpdate = GetReUpdateStatus()
                   };

            this.orderDetails = whOrdersDetails;
          
        }

        public string GetOrderId()
        {
            return orderItems.First().EbayOrder.OrderID;
        }

        public DateTime GetPurchasedDate()
        {
            return COnvertStringToDate(orderItems.First().PurchasedDate.ToString());
        }

        
       
        public string GetPlatform()
        {
            return "Ebay";
        }

        private DateTime COnvertStringToDate(string shipmentDate)
        {
            DateTime newShipmentDate = DateTime.Now;
            if (shipmentDate.Contains("."))
            {
                newShipmentDate = DateTime.Parse(shipmentDate, new CultureInfo("de-DE"));
            }
            else
            {
                newShipmentDate = DateTime.ParseExact(shipmentDate, "M/d/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);
            }

            return newShipmentDate;
        }

        public string GetAgentName()
        {
            return orderItems.First().Agentname;
        }

        public List<WareHouseOrdersDetailsEntity> GetItems()
        {
            WareHouseOrderDetails details = new WareHouseOrderDetails(this.orderItems);

            return details.Items;
        }


        public bool GetReUpdateStatus()
        {
            int count = this.Items.Where(x => x.SupplierOrderId == "").Count();

            if (count > 0) { return true; }
            else { return false; }
        }
    }
}
