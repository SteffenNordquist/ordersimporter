﻿using DN_Classes.DBCollection.SingleCalculatedOrders;
using DN_Classes.Entities;
using JTLOrdersGetDetails.Interfaces;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTLOrdersGetDetails.Classes
{
    public class AmazonDB : CalculatedDBCollection, IOrdersDB
    {
        public List<CalculatedProfitEntity> orderItems;
        
        public List<CalculatedProfitEntity> GetProductByOrderItem(string orderId)
        {
            return SingleCalculatedCollection.Find(Query.EQ("AmazonOrder.AmazonOrderId", orderId)).ToList();
        }


        public IOrders GetOrderType(string orderId)
        {
            this.orderItems = GetProductByOrderItem(orderId);

            if (this.orderItems != null && this.orderItems.Count() > 0)
            {
                return new Amazon(this.orderItems);
            }

            return null;
        }

        public List<CalculatedProfitEntity> order
        {
            get
            {
                return this.orderItems;
            }
           
        }

       
    }
}
