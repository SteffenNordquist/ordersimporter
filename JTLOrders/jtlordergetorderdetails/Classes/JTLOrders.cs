﻿using DN_Classes.Entities;
using DN_Classes.Entities.Warehouse;
using DN_Classes.Queries;
using DN_Classes.QueriesCommands.Warehouse_JTL_;
using JTLOrdersGetDetails.Interfaces;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTLOrdersGetDetails.Classes
{
    public class JTLOrders : WarehouseDBCollection, IJtlOrders
    {
        public List<JTLOrderEntity> GetJTLOrders()
        {
            var orders = new WareHouseQueries().GetItemsForDetails();

            return orders;
        }

        public void UpdateJtlOrder(string orderId, WareHouseOrdersEntity detail)
        {
            WareHouseCommands warehouseCommands = new WareHouseCommands();
            warehouseCommands.UpdateJtlOrdersPlus(orderId, detail);
        }


        public void UpdateJtlOrder(string orderId, WareHouseOrdersEntity_Ebay detail)
        {
            WareHouseCommands warehouseCommands = new WareHouseCommands();
            warehouseCommands.UpdateJtlOrdersPlus(orderId, detail);
        }
    }
}
