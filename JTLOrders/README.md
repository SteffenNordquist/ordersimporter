# README #

This README would normally document whatever steps are necessary to get your application up and running.


<UMLHERE>


### What is this repository for? ###

This repo contains three different JTLORDERS TOOLS

* **JTLORDERS** - gets the newest (orders within 30 days) from sql jtl and updates our jtlorders collection

* **JTLGETORDERDETAILS** - gets the order details of a certain order from singlecalculated item collection and updates the plus section of the jtlorders collection/documents (plus.orderDetails)

* **SINGLECALCULATED UPDATE** - updates the (calculatedprofit db) singlecalculated collection for shipped items ( plus.dVersandt)


### How do I get set up? ###

**JTLORDERS**

* Dependencies
     * mongodb driver
     * dnclasses ( application status)
     * ebay.service
* Database configuration
     *  JTLORDERS db -> jtlorders collection
* How to run tests
     * results can be found in jtlorders collection 
     * to know that the tool updates well, there should have orders which have order.dErstellt == DateTime.Now
* Deployment instructions
      * runs in 136.243.19.216 in Developers user account
       * exe can be found in C:\JtlOrders
       * not set on the task scheduler, only an infinite loop


**JTLORDERGETDEATILS**

* Dependencies
     * mongodb driver
     * dnclasses
     * ebay.service
* Database configuration
     *  from Calculated Profit DB -> SinglecalculatedItems collection nad EbayCalcultedItemsV2 colelction
     * to JTLORDERS db -> jtlorders collection
* How to run tests
     *  results can be found in jtlorders collection   
     *  to know that the tool updates well, there should have orders which have plus.orderDetails
* Deployment instructions
       * runs in 148.251.0.235 in Administrator user account
       * exe can be found in C:\jtlordergetorderDetails
       * set on task scheduler, runs every an hour


**SINGLEORDERUPDATER-JTLORDERS**

* Dependencies
     *  mongodb driver
     * dnclasses
     *  ebay.service
* Database configuration
     * from JTLORDERS db -> jtlorders collection
     * toCalculated Profit DB -> SinglecalculatedItems collection nad EbayCalcultedItemsV2 colelction
* How to run tests
     *  results can be found in SinglecalculatedItems and EbayCalcultedItemsV2  collection 
     * to know that the tool updates well, there should have orders which have plus.dVersandt
* Deployment instructions
      * runs in 148.251.0.235 in Administrator user account
      * exe can be found in C:\singleorderupdater-jtlorders
      * set on task scheduler, runs every an hour



**JTLCSVCreator**

* Tool Description
JTLCSVCreator, as the names suggests, creates a csv file that will be used in the SupplierUpdateSystem for JTL.

* Operation
JTLCSVCreator runs on the 136.243.60.22 as of 9/12/2015 (mm/dd/yyyy). The tool interfaces with the JTL software in getting the available stocks at the warehouse. It does that by starting a process called JTL-wawi-ameisei.exe. After Jtl-wawi-ameisei finished running, it will save a csv file (jtl_temp.csv). Lastly, the JTLCSVCreator copies that file in the wwwroot\csv folder. The entire process will repeat every 20 minutes.

* Common Errors
Here are some of the errors that we may encounter:
    * The JTL-wawi-ameisei.exe was not properly configured. Solution: Make sure that the parameters in JTL-wawi-ameisei is followed. JTL-wawi.ameisei.exe -s [path] -d [dbname] -u [username] -p [password] -t [export_template] -o [output]

    * Incorrect output of the CSV file. Solution: The export_template is wrong. Ask Eugen to verify if the export_template is correct.

    * Access Denied. Solution: Make sure that the tool is running under administrative privileges.

* AppStatus Name
JTLCSVCreator

* Dependencies
DN_Classes (Application Status)


### Who do I talk to? ###

* Repo owner or admin

      * **SINGLEORDERUPDATER-JTLORDERS** - Bless Largosa 
      * **JTLORDERGETDEATILS** - Bless Largosa
      * **JTLORDERS** - Jay Harold Reazol
      * **JTLCSVCreator- Jay Harold Reazol