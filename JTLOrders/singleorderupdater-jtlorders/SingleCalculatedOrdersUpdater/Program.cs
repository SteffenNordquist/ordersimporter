﻿using DN_Classes.AppStatus;
using DN_Classes.Queries;
using DN_Classes.QueriesCommands.EbaySingleCalculated;
using DN_Classes.QueriesCommands.SingleCalculatedOrders;
using DN_Classes.QueriesCommands.Warehouse_JTL_;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DN_Classes.JTL_Error;

namespace SingleCalculatedOrdersUpdater
{
    class Program
    {
        static void Main(string[] args)
        {

            new ShippedItemsUpdate();
            new ExcludeOrderingUpdate();
        }

    }
}
