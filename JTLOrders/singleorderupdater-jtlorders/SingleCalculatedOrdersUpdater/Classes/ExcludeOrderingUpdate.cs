﻿using DN_Classes.AppStatus;
using DN_Classes.Entities.Warehouse;
using DN_Classes.Logger;
using DN_Classes.Queries;
using DN_Classes.QueriesCommands.EbaySingleCalculated;
using DN_Classes.QueriesCommands.SingleCalculatedOrders;
using DN_Classes.QueriesCommands.Warehouse_JTL_;
using SingleCalculatedOrdersUpdater.Classes.SQL;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SingleCalculatedOrdersUpdater
{
    public class ExcludeOrderingUpdate
    {
        private string serverRunning { get; set; }
        private string toolName { get; set; }
        private string appStatusName { get; set; }
        private string logfilepath { get; set; }
        private string server { get; set; }
        private string database { get; set; }
        private string username { get; set; }
        private string password { get; set; }

        public ExcludeOrderingUpdate() {

            SetCredentials();
            ILogFileLocation logFileLocation = new DefaultLogFileLocation { LogDirectory = logfilepath, ToolName = toolName, SubToolName = toolName };


            using (ApplicationStatus appStatus = new ApplicationStatus(appStatusName))
            {
                LoggingConfig loggingConfig = new LoggingConfig();
                loggingConfig.Register(logFileLocation);

                using (ILogger logger = loggingConfig.Resolve<ILogger>())
                {
                    try
                    {
                        Console.WriteLine("Initializing ...");
                        var OrderIds = ExtractOrderIds(GetAllJtlOrders());
                        var ExcludedOrders = GetExcludedOrders(OrderIds);
                        UpdateSingleOrders(ExcludedOrders);
                    }
                    catch (Exception m)
                    {

                        appStatus.AddMessagLine(m.ToString());
                        AppLogger.Error(m.ToString());
                    }
                    appStatus.Successful();
                }
            }
        
        }

        private void SetCredentials()
        {
           this.serverRunning = Properties.Settings.Default.ServerRunning;
           this.toolName = "SingleCalculaTedUpdaterExcludeOrders";
           this.appStatusName = "JTLOrders TOOLS_" + toolName + "_" + serverRunning;
           this.logfilepath = Properties.Settings.Default.LogFilePath + toolName;
           this.server = Properties.Settings.Default.JTLServer;
           this.database = Properties.Settings.Default.JTLDatabaseName;
           this.username = Properties.Settings.Default.JTLUserName;
           this.password = Properties.Settings.Default.JTLPassword;
        }

        private List<string> GetExcludedOrders(List<string> orderIds)
        {
           
            AppLogger.Info("Number of Acquired Orders {0} : " + orderIds.Count);

            
            List<string> ExcludedOrders = new List<string>();
            int count = 0;
        
            SQLServer sqlserver = new SQLServer(server, username, password, database);

            foreach (string orderId in orderIds)
            {
                
                string query = " SELECT * FROM [eazybusiness].[dbo].[tBestellung] b, [eazybusiness].[dbo].[tgutschrift] g where [cInetBestellNr] = '" + orderId +"' and b.tRechnung_kRechnung = g.kRechnung";
                DataTable results = sqlserver.SelectCommand(query);
                if (results.Rows.Count > 0 && results.Select().First()["tRechnung_kRechnung"].ToString() != "0")
                {
                    count++;
                    ExcludedOrders.Add(orderId);
                    Console.WriteLine("Added to ExcludedList {0} - {1}", orderId, count);

                }
                
            }

            return ExcludedOrders;

        }

        public List<string> ExtractOrderIds(List<JTLOrderEntity> orders) {


            return orders.Select(x => x.order.cInetBestellNr).ToList();
        
        }

        public List<JTLOrderEntity> GetAllJtlOrders()
        {

            return new WareHouseQueries().GetAllOrdersWithin45Days();
        
        
        }

        private void UpdateSingleOrders(List<string> excludeFromOrderingOrders)
        {
            
            AppLogger.Info("Number of Excluded Orders {0} : "+ excludeFromOrderingOrders.Count);

           
            WareHouseCommands warehousecommands = new WareHouseCommands();


            foreach (var orderId in excludeFromOrderingOrders)
            {


                Regex regexAmazon = new Regex("[0-9]{3}-[0-9]{7}-[0-9]{7}");
                Match match = regexAmazon.Match(orderId);
                if (match.Success)
                {
                    new SingleCalculatedOrdersCommand().UpdateSingleOrdersExclude(orderId);
                    
                }
                else
                {
                    new EbaySingleCalculatedCommands().UpdateSingleOrdersExclude(orderId);
                }
               
                warehousecommands.UpdateJtlOrdersPlus_Exclude(orderId, true);

             
                AppLogger.Info("DB SET : Excluded From Ordering " + orderId);

            }

           

        }
    }
}
