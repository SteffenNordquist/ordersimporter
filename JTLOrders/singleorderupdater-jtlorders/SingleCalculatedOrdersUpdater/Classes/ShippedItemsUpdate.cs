﻿using DN_Classes.AppStatus;
using DN_Classes.Entities.Warehouse;
using DN_Classes.Logger;
using DN_Classes.Queries;
using DN_Classes.QueriesCommands.EbaySingleCalculated;
using DN_Classes.QueriesCommands.SingleCalculatedOrders;
using DN_Classes.QueriesCommands.Warehouse_JTL_;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SingleCalculatedOrdersUpdater
{
    public class ShippedItemsUpdate
    {
        private string serverRunning { get; set; }
        private string toolName { get; set; }
        private string appStatusName { get; set; }
        private string logfilepath { get; set; }
     

        public ShippedItemsUpdate() 
        {

            SetCredetials();
           
            ILogFileLocation logFileLocation = new DefaultLogFileLocation { LogDirectory = this.logfilepath, ToolName = toolName, SubToolName = toolName };
            
            using (ApplicationStatus appStatus = new ApplicationStatus(this.appStatusName))
            {
                LoggingConfig loggingConfig = new LoggingConfig();
                loggingConfig.Register(logFileLocation);

                using (ILogger logger = loggingConfig.Resolve<ILogger>())
                    {
                            try
                             {

                                 DoProcessShippedOrders();
                                
                                appStatus.Successful();
                             }
                            catch (Exception m)
                             {

                                   appStatus.AddMessagLine(m.ToString());
                                   AppLogger.Error(m.ToString());
                             }
                }
            }
        }

        private void DoProcessShippedOrders()
        {
            List<Entity> entityList = new List<Entity>();
            var itemShippedWithin30days = GetShippedOrders();
            AppLogger.Info("Orders to update : " + itemShippedWithin30days.Count);
            itemShippedWithin30days.ForEach(i => entityList.Add(new Entity { OrderId = i.order.cInetBestellNr, dVersandt = i.order.dVersandt }));
            UpdateSingleOrders(entityList);
        }

       

        private void SetCredetials()
        {

            this.serverRunning = Properties.Settings.Default.ServerRunning;
            this.toolName = "SingleCalculaTedUpdaterShippedOrders";
            this.appStatusName = "JTLOrders TOOLS_" + toolName + "_" + serverRunning;
            this.logfilepath = Properties.Settings.Default.LogFilePath + toolName;
            
        }

        private void UpdateSingleOrders(List<Entity> entityList)
        {
            SingleCalculatedOrdersCommand singleOrderCommand = new SingleCalculatedOrdersCommand();
            EbaySingleCalculatedCommands singleOrderCommandEbay = new EbaySingleCalculatedCommands();
            WareHouseCommands warehousecommands = new WareHouseCommands();


            foreach (var entity in entityList)
            {


                string orderId = entity.OrderId;
                string dVersandt = entity.dVersandt.ToString();

                Regex regexAmazon = new Regex("[0-9]{3}-[0-9]{7}-[0-9]{7}");
                Match match = regexAmazon.Match(orderId);
                if (match.Success)
                {

                    singleOrderCommand.UpdateSingleOrders(orderId, dVersandt);
                }
                else
                {

                    singleOrderCommandEbay.UpdateSingleOrders(orderId, dVersandt);

                }
                warehousecommands.UpdateJtlOrdersPlus_Exclude(entity.OrderId, true);

            

                AppLogger.Info("SHIPPED Orders Excluded " + entity.OrderId + " - " + entity.dVersandt);
              

            }
        }

        public List<JTLOrderEntity> GetShippedOrders() {

            return new WareHouseQueries().GetItemsShippedWithin30days();

        }

        private static void UpdateBothCalculatedJtl(List<string> amazonOrderIds)
        {
            SingleCalculatedOrdersCommand singleOrderCommand = new SingleCalculatedOrdersCommand();
            WareHouseCommands warehousecommands = new WareHouseCommands();
            foreach (var x in amazonOrderIds)
            {

                warehousecommands.UnSetJtlOrdersPlus(x);
                //  singleOrderCommand.UpdateSingleOrdersUnset(x);

                Console.WriteLine(x);
            }
        }
    }
}
