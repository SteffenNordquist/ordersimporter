﻿using DN_Classes.AppStatus;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DN_Classes.JTL_Error;

namespace CSVImportJTL
{

    class Program
    {
        static void Main(string[] args)
        {
            ApplicationStatus appStatus = new ApplicationStatus("JTL TOOLS_JTLCSVCreator");
            try
            {
        
            appStatus.Start();
            Console.WriteLine("Opening JTLWawi");
            //start the JTL-wawi-ameise.exe to start exporting. 
            var process = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = @"C:\Progra~2\JTL-Software\JTL-wawi-ameise.exe",
                    Arguments = @"-s localhost -d eazybusiness -u sa -p JTL2015! -t EXP26 -o C:\inetpub\wwwroot\csv\jtl_temp.csv"
                }
            };
            process.Start();
            //wait for the process to complete
            process.WaitForExit();
            Console.WriteLine("Copying output");
            //since the SupplierUpdateSystem uses jtl.csv, copy the output as jtl.csv so that
            //the server can use the updated file.
            File.Copy(@"C:\inetpub\wwwroot\csv\jtl_temp.csv", @"C:\inetpub\wwwroot\csv\jtl.csv", true);
            Console.WriteLine("Sleep");
            appStatus.Successful();
            appStatus.Stop();
            }
            catch (Exception ex)
            {
                appStatus.AddMessagLine(ex.StackTrace);
                JTLErrorLog log = new JTLErrorLog();
                log.AddError(ex.StackTrace, "JTLCSVCreator");
            }
        }

        private static string GetElapsedTimeString(Stopwatch _stopWatch)
        {
            TimeSpan elapsedTime = _stopWatch.Elapsed;

            string executionTimeSpan = elapsedTime.Days + "d : " + elapsedTime.Hours + "h : " + elapsedTime.Minutes + "m : " + elapsedTime.Seconds + "s : " + elapsedTime.Milliseconds + "ms";
            return executionTimeSpan;
        }
    }
}
