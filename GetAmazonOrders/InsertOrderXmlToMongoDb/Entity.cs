﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.GridFS;
using MongoDB.Driver.Linq;

namespace MongoApplication
{
    class Entity
    {
        public ObjectId Id { get; set; }
        public BsonDocument orderdetail { get; set; }
    }
}
