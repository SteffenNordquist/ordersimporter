﻿using MongoApplication;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

using InvoiceGenerator;
using OrderData;

namespace InsertOrderXmlToMongoDb
{
    public class Inserter
    {

        
        public static MongoCollection<AgentEntity> AgentCollection;
        public static MongoCollection<InvoiceData> InvoiceDataCollection;
        public static MongoCollection<AmazonOrder> AmazonOrderCollection;

        public string OrderXML = string.Empty;
        string agent = "";
        int agentid;

        void SetCollections()
        {
            string ConnectionString = @"mongodb://client144:client144devnetworks@144.76.166.207/Orders";
            MongoClient Client = new MongoClient(ConnectionString);
            MongoServer Server = Client.GetServer();
            MongoDatabase Database = Server.GetDatabase("Orders");
            AgentCollection = Database.GetCollection<AgentEntity>("agents");
            InvoiceDataCollection = Database.GetCollection<InvoiceData>("invoices");
            AmazonOrderCollection = Database.GetCollection<AmazonOrder>("orderdetails");
        }

        public Inserter(string orderXml,int agentid)
        {
            SetCollections();

            OrderXML = orderXml;
            //this.agent = agent;
            this.agentid = agentid;
            insert();
        }


        public void insert()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(OrderXML);


            foreach (XmlNode node in doc.DocumentElement.ChildNodes)
            {
                string jsonText = JsonConvert.SerializeXmlNode(node);
                MongoDB.Bson.BsonDocument order = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(jsonText);

                string amazonOrderId = node.SelectSingleNode("./AmazonOrderId").InnerText; 

                var query = Query.EQ("orderdetail.Order.AmazonOrderId", amazonOrderId);

                BsonDocument orderdetail = order;
                orderdetail["agent"] = agentid;
              
                var update = Update.Set("orderdetail", orderdetail);
                WriteConcernResult wcr = AmazonOrderCollection.Update(query, update, UpdateFlags.Upsert);

                InsertToInvoiceData(orderdetail);

               
            }

        }



        #region Insert to invoice collection
        public void InsertToInvoiceData(BsonDocument orderdetail)
        {
            AmazonOrder order = new AmazonOrder { orderdetail = orderdetail };

            //string agentName = orderdetail["agent"].AsString;
            string amazonOrderId = order.GetPlatformOrderId();

            int invoiceID = InvoiceID(agentid, "Amazon");
            DateTime purchasedate = order.GetPurchaseDate();
            Address customer = order.GetCustomer();
            Address billing = order.GetBillingAddress();
            List<OrderItem> orderItems = order.GetOrderItems();
            double totalShipping = order.GetTotalShipping();

            Order newOrder = new Order
            {
                orderItemList = orderItems,
                invoiceId = invoiceID,
                amazonOrderId = amazonOrderId,
                shipping = totalShipping,
                shippingAddress = customer,
                billingAddress = billing,
                paymentType = "bezahlt",
                orderedFromPlatform = "Amazon",
                purchasedDate = purchasedate,
                LastChangeTime = order.GetLastChangeTime(),
                sellerFee = order.GetSellerFee(),
                moneyTransactionFee = order.GetMoneyTransactionFee(),
                PaidTime = purchasedate

            };

            //InvoiceData i = new InvoiceData { reference = amazonOrderId, order = newOrder, agent = GetAgent(agentName) };
            
            Agent agent = new Agent { agentID = agentid};
            var update = Update.Set("order", newOrder.ToBsonDocument()).Set("agent", agent.ToBsonDocument());
            InvoiceDataCollection.Update(Query.EQ("reference", amazonOrderId), update, UpdateFlags.Upsert);

            UpdateOrderItem(amazonOrderId, invoiceID);
        }

        Agent GetAgent(string agentname)
        {
            //var agentFromDB = AgentCollection.FindOne(Query.EQ("agent.amazonname", agentname));
            //if (agentFromDB == null)
            //{
             var   agentFromDB = AgentCollection.FindOne(Query.EQ("agent.agentname",agentname));
            //}

            if (agentFromDB != null)
            {
                //string agentLogoURL = agentFromDB.GetAgentLogoURL();

                //if (agentLogoURL != "")
                //{
                //    agentLogoURL = "http://148.251.0.235/AmazonOrder/AmazonOrders/Images" + agentLogoURL.Replace("~/images/", "/");
                //}

                Agent agent = new Agent
                {   
                    agentID = agentFromDB.agent["agentId"].AsInt32,
                    agentLogoFilePath = "",
                    sendBackAddress = "Versandlager für Bücher und Medien|Dorotheenstraße 30, 10117 Berlin",
                    companyName = agentname,
                    financialAuthorityString = agentFromDB.GetFinancialAuthority()
                };
                agent.address = new Address(agentname,"", agentFromDB.agent["address1"].AsString,agentFromDB.agent["address2"].AsString, agentFromDB.agent["postcode"].AsString, agentFromDB.agent["city"].AsString, "", "", "");

                return agent;
            }

            return null;
        }

        int InvoiceID(int agentid, string platform)
        {
            int invoiceID = 0;

            if (InvoiceDataCollection.Find(Query.And(Query.EQ("agent.agentID", agentid), Query.EQ("order.orderedFromPlatform", platform))).Count() > 0)
            {
                var max = InvoiceDataCollection.Find(Query.And(Query.EQ("agent.agentID", agentid), Query.EQ("order.orderedFromPlatform", "Amazon"))).SetSortOrder(SortBy.Descending("order.invoiceId")).SetLimit(1).FirstOrDefault();
                invoiceID = Convert.ToInt32(max.order.invoiceId) + 1;
            }


            return invoiceID;
        }

        void UpdateOrderItem(string amazonid, int invoiceID)
        {
            UpdateBuilder update = MongoDB.Driver.Builders.Update.Set("invoiceid", invoiceID);
            AmazonOrderCollection.Update(Query.EQ("orderdetail.Order.AmazonOrderId", amazonid), update);
        }

        #endregion
    }
}
