/******************************************************************************* 
 *  Copyright 2008-2012 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *  Licensed under the Apache License, Version 2.0 (the "License"); 
 *  
 *  You may not use this file except in compliance with the License. 
 *  You may obtain a copy of the License at: http://aws.amazon.com/apache2.0
 *  This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
 *  CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 *  specific language governing permissions and limitations under the License.
 * ***************************************************************************** 
 * 
 *  Marketplace Web Service Orders CSharp Library
 *  API Version: 2011-01-01
 * 
 */


using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using MarketplaceWebServiceOrders;
using MarketplaceWebServiceOrders.Mock;
using MarketplaceWebServiceOrders.Model;
using System.IO;
using System.Diagnostics;
using System.Text;
using InsertOrderXmlToMongoDb;
using ReadWriteRegisterKey;
namespace MarketplaceWebServiceOrders.Samples
{

    /// <summary>
    /// Marketplace Web Service Orders  Samples
    /// </summary>
    public class MarketplaceWebServiceOrdersSamples 
    {
    
       /**
        * Samples for Marketplace Web Service Orders functionality
        */
        static String accessKeyId = "";
        static String secretAccessKey = "";
        static String merchantId = "";
        static String marketplaceId = "";
        //accesskeyid AKIAJVSUPLN7ZTDEE5UQ
        //secret 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
        //merchantid A34EK1L21G1YHD
        //marketplaceid A1PA6795UKMFR9
        public static void Main(string[] args)
        {


            if (Amazon.HasKeyValues())
            {
                accessKeyId = Amazon.accesskeyid;
                secretAccessKey = Amazon.secretaccesskeyid;
                merchantId = Amazon.merchantid;
                marketplaceId = Amazon.marketplaceid;
                /************************************************************************
                  * Access Key ID and Secret Access Key ID
                 ***********************************************************************/


                /************************************************************************
                 * The application name and version are included in each MWS call's
                 * HTTP User-Agent field.
                 ***********************************************************************/
                const string applicationName = "a";
                const string applicationVersion = "0.00";



                MarketplaceWebServiceOrdersConfig config = new MarketplaceWebServiceOrdersConfig();

                // Europe:
                config.ServiceURL = "https://mws-eu.amazonservices.com/Orders/2011-01-01";


                /************************************************************************
                * Instantiate  Implementation of Marketplace Web Service Orders  
                ***********************************************************************/
                MarketplaceWebServiceOrdersClient service = new MarketplaceWebServiceOrdersClient(
                    applicationName, applicationVersion, accessKeyId, secretAccessKey, config);


                StringBuilder sb = new StringBuilder();

                sb.AppendLine("<OrderList>");
                sb.AppendLine(OrderFetcherSample.InvokeOrderFetcherSample(service, merchantId, new string[] { marketplaceId }));
                sb.AppendLine("</OrderList>");

                insertOrdersToDb(sb.ToString());
            }
        }

        public static void insertOrdersToDb(string xml) {
            Inserter inserter = new Inserter(xml, Properties.Settings.Default.AgentName); 
        }
    }
}
