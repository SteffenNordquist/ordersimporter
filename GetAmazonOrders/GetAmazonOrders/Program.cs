﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using MarketplaceWebServiceOrders;
using MarketplaceWebServiceOrders.Mock;
using MarketplaceWebServiceOrders.Model;
using System.IO;
using System.Diagnostics;
using System.Text;
using InsertOrderXmlToMongoDb;
using ReadWriteRegisterKey;

using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;
using MongoDB.Bson.Serialization;
using AgentDLL;
using MongoDB.Bson.IO;
using Newtonsoft.Json;

namespace GetAmazonOrders
{
    class Program
    {
        static String accessKeyId = "";
        static String secretAccessKey = "";
        static String merchantId = "";
        static String marketplaceId = "";

        static MongoCollection<Agent> AgentCollection;

        
        static void Main(string[] args)
        {

            AmazonTokens agent = Agent.GetAmazonTokens();

                if (agent != null)
                {
                    accessKeyId = agent.accesskeyid;
                    secretAccessKey = agent.secretaccesskeyid;
                    merchantId = agent.merchantId;
                    marketplaceId = agent.marketplaceId;

                    const string applicationName = "a";
                    const string applicationVersion = "0.00";



                    MarketplaceWebServiceOrdersConfig config = new MarketplaceWebServiceOrdersConfig();

                    // Europe:
                    config.ServiceURL = "https://mws-eu.amazonservices.com/Orders/2011-01-01";


                    /************************************************************************
                    * Instantiate  Implementation of Marketplace Web Service Orders  
                    ***********************************************************************/
                    MarketplaceWebServiceOrdersClient service = new MarketplaceWebServiceOrdersClient(
                        applicationName, applicationVersion, accessKeyId, secretAccessKey, config);


                    StringBuilder sb = new StringBuilder();

                    sb.AppendLine("<OrderList>");
                    sb.AppendLine(OrderFetcherSample.InvokeOrderFetcherSample(service, merchantId, new string[] { marketplaceId }));
                    sb.AppendLine("</OrderList>");

                    insertOrdersToDb(sb.ToString(),agent.agentID);

                }
           
        }
        static void insertOrdersToDb(string xml, int agentid)
        {
            Inserter inserter = new Inserter(xml, agentid);
        }
    }
}
