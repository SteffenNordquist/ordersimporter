﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
namespace GetAmazonOrders
{
    public class Agent
    {
        public ObjectId id { get; set; }
        public BsonDocument agent { get; set; }
    }
}
