﻿using BestellImport.DnOrderFormat;
using DN_Classes.Agent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BestellImport.JTL_Import
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234")]
    [System.SerializableAttribute()]
 //   [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class tBestellung
    {
        private sprache cSpracheField;
        private waehrung cWaehrungField;
        private decimal fGuthabenField;
        private decimal fGesamtsummeField;
        private string cBestellNrField;
        private string cExterneBestellNrField;
        private string cVersandartNameField;
        private string cVersandInfoField;
        private string dVersandDatumField;
        private string cTrackingField;
        private string cLogistikerField;
        private string dLieferDatumField;
        private string cKommentarField;
        private string cBemerkungField;
        private System.DateTime dErstelltField;
        private string cZahlungsartNameField;
        private string dBezahltDatumField;
        private decimal fBezahltField;
        private tWarenkorbPos[] twarenkorbposField;
        private tKunde tkundeField;
        private tLieferadresse tlieferadresseField;
        private tZahlungsInfo tzahlungsinfoField;
        private ulong kFirmaField;
        private bool kFirmaFieldSpecified;
        private ulong kBenutzerField;
        private bool kBenutzerFieldSpecified;

        public tBestellung(IOrder order)
        {
            //order.cBemerkung = "bem";
            dErstellt = DateTime.Now;

            cBestellNr = order.getOrderId();
            cKommentar = "";
            cBemerkung = new Agent(int.Parse(order.getAgentId())).agentEntity.agent.companyname + " " + order.GetPlatformName()+" "+DateTime.Now.Ticks;
            cLogistiker = "";
            cSprache = sprache.ger;
            cTracking = "";
            cVersandartName = "";
            cVersandInfo = "";
            cWaehrung = waehrung.EUR;
            cZahlungsartName = order.getPaymentMethod().ToString();
            
         //   dBezahltDatum = "";
            
            dLieferDatum = "";
            dVersandDatum = "";

            fBezahlt = (decimal)order.getAmountPaid();
            fGesamtsumme = (decimal)order.getTotalAmount();
            //fGuthaben = (decimal)0.0;
            //order.kBenutzer = 


            //cVersandartNameField = "DHL Paket";

            kBenutzerField = 7;
            kBenutzerFieldSpecified = true; 


            tKunde kunde = new tKunde();
            kunde.cKundenNr = "";
            kunde.cVorname = "";
            kunde.cNachname = (order.getFirstName() + " " + order.getLastName()).Trim().Replace("'", "''");
            kunde.cPLZ = order.getCustomer().getZipCode();
            kunde.cOrt = order.getCustomer().getCity();
            kunde.cFirma = order.getCustomer().getCompany();
            kunde.cStrasse = order.getCustomer().getStreet();
            kunde.cAdressZusatz = order.getCustomer().getAddtionalAddressAttribute();
            kunde.cTel = order.getCustomer().getPhoneNumber();
            kunde.cLand = order.getCountry();
            //kunde.cTitel = "Herr";

            kunde.cWWW = order.getCustomer().getEmail() ;
            kunde.dErstellt = order.getOrderDate();
            kunde.cNewsletter = yesno.N;
            kunde.cMail = order.getCustomer().getEmail();
            kunde.cAnrede = "";
            kunde.cTitel = "";
            kunde.cFax = "";
            kunde.cUSTID = ""; 

            tkunde = kunde;

            

            tLieferadresse shippingAddress = new tLieferadresse(order.getShippingAddress());
            tlieferadresseField = shippingAddress;
            

            List<tWarenkorbPos> orderItems = new List<tWarenkorbPos>();
            foreach (var orderItem in order.getOrderItmes())
            {
                orderItems.Add(new tWarenkorbPos(orderItem));
            }


            var taxMajority = orderItems.GroupBy(x => x.fMwSt).Select(x =>new KeyValuePair<decimal, decimal>(x.Key, x.Sum(y=> y.fPreisEinzelNetto))).OrderByDescending(x => x.Value).First().Key;

            if ((decimal)order.getShippingIncome() > 0)
            {
                tWarenkorbPos shippingPos = new tWarenkorbPos();
                shippingPos.cBarcode = "";
                shippingPos.fPreisEinzelNetto = (decimal)order.getShippingIncome() / ((100 + taxMajority) / 100); 
                shippingPos.fPreis = Math.Round((decimal)order.getShippingIncome(),2);
                shippingPos.fMwSt = (decimal)taxMajority;
                shippingPos.cArtNr = "21";
                shippingPos.fAnzahl = 1;
                shippingPos.cName = "Versand";
                //orderItem.kArtikel = 352881;

                orderItems.Add(shippingPos);
            }



            twarenkorbpos = orderItems.ToArray();
            cExterneBestellNr = order.getOrderId();

            kFirmaField = ulong.Parse(order.getExternalCompanyId());
            kFirmaFieldSpecified = true; 
        }


        public tBestellung()
        {
        }

        /// <remarks/>
        public sprache cSprache
        {
            get
            {
                return this.cSpracheField;
            }
            set
            {
                this.cSpracheField = value;
            }
        }
        /// <remarks/>
        public waehrung cWaehrung
        {
            get
            {
                return this.cWaehrungField;
            }
            set
            {
                this.cWaehrungField = value;
            }
        }
        /// <remarks/>
        public decimal fGuthaben
        {
            get
            {
                return this.fGuthabenField;
            }
            set
            {
                this.fGuthabenField = value;
            }
        }
        /// <remarks/>
        public decimal fGesamtsumme
        {
            get
            {
                return this.fGesamtsummeField;
            }
            set
            {
                this.fGesamtsummeField = value;
            }
        }
        /// <remarks/>
        public string cBestellNr
        {
            get
            {
                return this.cBestellNrField;
            }
            set
            {
                this.cBestellNrField = value;
            }
        }
        /// <remarks/>
        public string cExterneBestellNr
        {
            get
            {
                return this.cExterneBestellNrField;
            }
            set
            {
                this.cExterneBestellNrField = value;
            }
        }
        /// <remarks/>
        public string cVersandartName
        {
            get
            {
                return this.cVersandartNameField;
            }
            set
            {
                this.cVersandartNameField = value;
            }
        }
        /// <remarks/>
        public string cVersandInfo
        {
            get
            {
                return this.cVersandInfoField;
            }
            set
            {
                this.cVersandInfoField = value;
            }
        }
        /// <remarks/>
        public string dVersandDatum
        {
            get
            {
                return this.dVersandDatumField;
            }
            set
            {
                this.dVersandDatumField = value;
            }
        }
        /// <remarks/>
        public string cTracking
        {
            get
            {
                return this.cTrackingField;
            }
            set
            {
                this.cTrackingField = value;
            }
        }
        /// <remarks/>
        public string cLogistiker
        {
            get
            {
                return this.cLogistikerField;
            }
            set
            {
                this.cLogistikerField = value;
            }
        }
        /// <remarks/>
        public string dLieferDatum
        {
            get
            {
                return this.dLieferDatumField;
            }
            set
            {
                this.dLieferDatumField = value;
            }
        }
        /// <remarks/>
        public string cKommentar
        {
            get
            {
                return this.cKommentarField;
            }
            set
            {
                this.cKommentarField = value;
            }
        }
        /// <remarks/>
        public string cBemerkung
        {
            get
            {
                return this.cBemerkungField;
            }
            set
            {
                this.cBemerkungField = value;
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime dErstellt
        {
            get
            {
                return this.dErstelltField;
            }
            set
            {
                this.dErstelltField = value;
            }
        }
        /// <remarks/>
        public string cZahlungsartName
        {
            get
            {
                return this.cZahlungsartNameField;
            }
            set
            {
                this.cZahlungsartNameField = value;
            }
        }
        /// <remarks/>
        public string dBezahltDatum
        {
            get
            {
                return this.dBezahltDatumField;
            }
            set
            {
                this.dBezahltDatumField = value;
            }
        }
        /// <remarks/>
        public decimal fBezahlt
        {
            get
            {
                return this.fBezahltField;
            }
            set
            {
                this.fBezahltField = value;
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("twarenkorbpos")]
        public tWarenkorbPos[] twarenkorbpos
        {
            get
            {
                return this.twarenkorbposField;
            }
            set
            {
                this.twarenkorbposField = value;
            }
        }
        /// <remarks/>
        public tKunde tkunde
        {
            get
            {
                return this.tkundeField;
            }
            set
            {
                this.tkundeField = value;
            }
        }
        /// <remarks/>
        public tLieferadresse tlieferadresse
        {
            get
            {
                return this.tlieferadresseField;
            }
            set
            {
                this.tlieferadresseField = value;
            }
        }
        /// <remarks/>
        public tZahlungsInfo tzahlungsinfo
        {
            get
            {
                return this.tzahlungsinfoField;
            }
            set
            {
                this.tzahlungsinfoField = value;
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ulong kFirma
        {
            get
            {
                return this.kFirmaField;
            }
            set
            {
                this.kFirmaField = value;
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool kFirmaSpecified
        {
            get
            {
                return this.kFirmaFieldSpecified;
            }
            set
            {
                this.kFirmaFieldSpecified = value;
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ulong kBenutzer
        {
            get
            {
                return this.kBenutzerField;
            }
            set
            {
                this.kBenutzerField = value;
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool kBenutzerSpecified
        {
            get
            {
                return this.kBenutzerFieldSpecified;
            }
            set
            {
                this.kBenutzerFieldSpecified = value;
            }
        }
    }
}
