﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BestellImport.JTL_Import
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class tKundenAttribute
    {
        private string cNameField;
        private string cWertField;
        /// <remarks/>
        public string cName
        {
            get
            {
                return this.cNameField;
            }
            set
            {
                this.cNameField = value;
            }
        }
        /// <remarks/>
        public string cWert
        {
            get
            {
                return this.cWertField;
            }
            set
            {
                this.cWertField = value;
            }
        }
    }
}
