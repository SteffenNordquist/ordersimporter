﻿using BestellImport.DnOrderFormat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BestellImport.JTL_Import
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234")]
    [System.SerializableAttribute()]
  //  [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class tBestellungen
    {

        public tBestellungen(List<IOrder> orders)
        {
            List<tBestellung> orderList = new List<tBestellung>();

            foreach (var order in orders.Where(x => x.getOrderStatus() != OrderStatus.cancelled))
            {
                orderList.Add(new tBestellung(order));
            }

            tBestellung = orderList.ToArray();
        }


        public tBestellungen()
        {
        }

        private tBestellung[] tBestellungField;
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("tBestellung")]
        public tBestellung[] tBestellung
        {
            get
            {
                return this.tBestellungField;
            }
            set
            {
                this.tBestellungField = value;
            }
        }
    }
}
