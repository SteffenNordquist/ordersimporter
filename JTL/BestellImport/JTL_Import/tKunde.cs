﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BestellImport.JTL_Import
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class tKunde
    {
        private string cKundenNrField;
        private string cAnredeField;
        private string cTitelField;
        private string cVornameField;
        private string cNachnameField;
        private string cFirmaField;
        private string cStrasseField;
        private string cAdressZusatzField;
        private string cPLZField;
        private string cOrtField;
        private string cBundeslandField;
        private string cLandField;
        private string cTelField;
        private string cMobilField;
        private string cFaxField;
        private string cMailField;
        private string cUSTIDField;
        private string cWWWField;
        private yesno cNewsletterField;
        private System.DateTime dGeburtstagField;
        private decimal fRabattField;
        private string cHerkunftField;
        private System.DateTime dErstelltField;
        private tKundenAttribute tkundenattributeField;
        private ulong kKundeField;
        private bool kKundeFieldSpecified;
        private ulong kKundenKategorieField;
        private bool kKundenKategorieFieldSpecified;
        private ulong kKundenGruppeField;
        private bool kKundenGruppeFieldSpecified;
        private ulong kSpracheField;
        private bool kSpracheFieldSpecified;
        /// <remarks/>
        public string cKundenNr
        {
            get
            {
                return this.cKundenNrField;
            }
            set
            {
                this.cKundenNrField = value;
            }
        }
        /// <remarks/>
        public string cAnrede
        {
            get
            {
                return this.cAnredeField;
            }
            set
            {
                this.cAnredeField = value;
            }
        }
        /// <remarks/>
        public string cTitel
        {
            get
            {
                return this.cTitelField;
            }
            set
            {
                this.cTitelField = value;
            }
        }
        /// <remarks/>
        public string cVorname
        {
            get
            {
                return this.cVornameField;
            }
            set
            {
                this.cVornameField = value;
            }
        }
        /// <remarks/>
        public string cNachname
        {
            get
            {
                return this.cNachnameField;
            }
            set
            {
                this.cNachnameField = value;
            }
        }
        /// <remarks/>
        public string cFirma
        {
            get
            {
                return this.cFirmaField;
            }
            set
            {
                this.cFirmaField = value;
            }
        }
        /// <remarks/>
        public string cStrasse
        {
            get
            {
                return this.cStrasseField;
            }
            set
            {
                this.cStrasseField = value;
            }
        }
        /// <remarks/>
        public string cAdressZusatz
        {
            get
            {
                return this.cAdressZusatzField;
            }
            set
            {
                this.cAdressZusatzField = value;
            }
        }
        /// <remarks/>
        public string cPLZ
        {
            get
            {
                return this.cPLZField;
            }
            set
            {
                this.cPLZField = value;
            }
        }
        /// <remarks/>
        public string cOrt
        {
            get
            {
                return this.cOrtField;
            }
            set
            {
                this.cOrtField = value;
            }
        }
        /// <remarks/>
        public string cBundesland
        {
            get
            {
                return this.cBundeslandField;
            }
            set
            {
                this.cBundeslandField = value;
            }
        }
        /// <remarks/>
        public string cLand
        {
            get
            {
                return this.cLandField;
            }
            set
            {
                this.cLandField = value;
            }
        }
        /// <remarks/>
        public string cTel
        {
            get
            {
                return this.cTelField;
            }
            set
            {
                this.cTelField = value;
            }
        }
        /// <remarks/>
        public string cMobil
        {
            get
            {
                return this.cMobilField;
            }
            set
            {
                this.cMobilField = value;
            }
        }
        /// <remarks/>
        public string cFax
        {
            get
            {
                return this.cFaxField;
            }
            set
            {
                this.cFaxField = value;
            }
        }
        /// <remarks/>
        public string cMail
        {
            get
            {
                return this.cMailField;
            }
            set
            {
                this.cMailField = value;
            }
        }
        /// <remarks/>
        public string cUSTID
        {
            get
            {
                return this.cUSTIDField;
            }
            set
            {
                this.cUSTIDField = value;
            }
        }
        /// <remarks/>
        public string cWWW
        {
            get
            {
                return this.cWWWField;
            }
            set
            {
                this.cWWWField = value;
            }
        }
        /// <remarks/>
        public yesno cNewsletter
        {
            get
            {
                return this.cNewsletterField;
            }
            set
            {
                this.cNewsletterField = value;
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime dGeburtstag
        {
            get
            {
                return this.dGeburtstagField;
            }
            set
            {
                this.dGeburtstagField = value;
            }
        }
        /// <remarks/>
        public decimal fRabatt
        {
            get
            {
                return this.fRabattField;
            }
            set
            {
                this.fRabattField = value;
            }
        }
        /// <remarks/>
        public string cHerkunft
        {
            get
            {
                return this.cHerkunftField;
            }
            set
            {
                this.cHerkunftField = value;
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime dErstellt
        {
            get
            {
                return this.dErstelltField;
            }
            set
            {
                this.dErstelltField = value;
            }
        }
        /// <remarks/>
        public tKundenAttribute tkundenattribute
        {
            get
            {
                return this.tkundenattributeField;
            }
            set
            {
                this.tkundenattributeField = value;
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ulong kKunde
        {
            get
            {
                return this.kKundeField;
            }
            set
            {
                this.kKundeField = value;
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool kKundeSpecified
        {
            get
            {
                return this.kKundeFieldSpecified;
            }
            set
            {
                this.kKundeFieldSpecified = value;
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ulong kKundenKategorie
        {
            get
            {
                return this.kKundenKategorieField;
            }
            set
            {
                this.kKundenKategorieField = value;
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool kKundenKategorieSpecified
        {
            get
            {
                return this.kKundenKategorieFieldSpecified;
            }
            set
            {
                this.kKundenKategorieFieldSpecified = value;
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ulong kKundenGruppe
        {
            get
            {
                return this.kKundenGruppeField;
            }
            set
            {
                this.kKundenGruppeField = value;
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool kKundenGruppeSpecified
        {
            get
            {
                return this.kKundenGruppeFieldSpecified;
            }
            set
            {
                this.kKundenGruppeFieldSpecified = value;
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ulong kSprache
        {
            get
            {
                return this.kSpracheField;
            }
            set
            {
                this.kSpracheField = value;
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool kSpracheSpecified
        {
            get
            {
                return this.kSpracheFieldSpecified;
            }
            set
            {
                this.kSpracheFieldSpecified = value;
            }
        }
    }
}
