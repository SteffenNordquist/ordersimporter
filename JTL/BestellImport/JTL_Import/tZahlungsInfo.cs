﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BestellImport.JTL_Import
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class tZahlungsInfo
    {
        private string cBankNameField;
        private string cBLZField;
        private string cKontoNrField;
        private string cKartenNrField;
        private string dGueltigkeitField;
        private string cCVVField;
        private string cKartenTypField;
        private string cInhaberField;
        /// <remarks/>
        public string cBankName
        {
            get
            {
                return this.cBankNameField;
            }
            set
            {
                this.cBankNameField = value;
            }
        }
        /// <remarks/>
        public string cBLZ
        {
            get
            {
                return this.cBLZField;
            }
            set
            {
                this.cBLZField = value;
            }
        }
        /// <remarks/>
        public string cKontoNr
        {
            get
            {
                return this.cKontoNrField;
            }
            set
            {
                this.cKontoNrField = value;
            }
        }
        /// <remarks/>
        public string cKartenNr
        {
            get
            {
                return this.cKartenNrField;
            }
            set
            {
                this.cKartenNrField = value;
            }
        }
        /// <remarks/>
        public string dGueltigkeit
        {
            get
            {
                return this.dGueltigkeitField;
            }
            set
            {
                this.dGueltigkeitField = value;
            }
        }
        /// <remarks/>
        public string cCVV
        {
            get
            {
                return this.cCVVField;
            }
            set
            {
                this.cCVVField = value;
            }
        }
        /// <remarks/>
        public string cKartenTyp
        {
            get
            {
                return this.cKartenTypField;
            }
            set
            {
                this.cKartenTypField = value;
            }
        }
        /// <remarks/>
        public string cInhaber
        {
            get
            {
                return this.cInhaberField;
            }
            set
            {
                this.cInhaberField = value;
            }
        }
    }
}
