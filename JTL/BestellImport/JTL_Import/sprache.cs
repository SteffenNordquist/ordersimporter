﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BestellImport.JTL_Import
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public enum sprache
    {
        /// <remarks/>
        ger,
        /// <remarks/>
        eng,
        /// <remarks/>
        dan,
        /// <remarks/>
        fin,
        /// <remarks/>
        fre,
        /// <remarks/>
        geo,
        /// <remarks/>
        gre,
        /// <remarks/>
        gle,
        /// <remarks/>
        ice,
        /// <remarks/>
        ita,
        /// <remarks/>
        scr,
        /// <remarks/>
        dut,
        /// <remarks/>
        nor,
        /// <remarks/>
        pol,
        /// <remarks/>
        por,
        /// <remarks/>
        swe,
        /// <remarks/>
        slo,
        /// <remarks/>
        slv,
        /// <remarks/>
        spa,
        /// <remarks/>
        cze,
        /// <remarks/>
        tur,
    }
}
