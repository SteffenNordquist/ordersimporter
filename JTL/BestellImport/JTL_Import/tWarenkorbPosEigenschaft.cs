﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BestellImport.JTL_Import
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class tWarenkorbPosEigenschaft
    {
        private string cArtNrField;
        private string cBarcodeField;
        private string cEigenschaftNameField;
        private string cEigenschaftWertNameField;
        private string cFreifeldWertField;
        private decimal fAufpreisField;
        private string[] textField;
        private ulong kEigenschaftField;
        private bool kEigenschaftFieldSpecified;
        private ulong kEigenschaftWertField;
        private bool kEigenschaftWertFieldSpecified;
        /// <remarks/>
        public string cArtNr
        {
            get
            {
                return this.cArtNrField;
            }
            set
            {
                this.cArtNrField = value;
            }
        }
        /// <remarks/>
        public string cBarcode
        {
            get
            {
                return this.cBarcodeField;
            }
            set
            {
                this.cBarcodeField = value;
            }
        }
        /// <remarks/>
        public string cEigenschaftName
        {
            get
            {
                return this.cEigenschaftNameField;
            }
            set
            {
                this.cEigenschaftNameField = value;
            }
        }
        /// <remarks/>
        public string cEigenschaftWertName
        {
            get
            {
                return this.cEigenschaftWertNameField;
            }
            set
            {
                this.cEigenschaftWertNameField = value;
            }
        }
        /// <remarks/>
        public string cFreifeldWert
        {
            get
            {
                return this.cFreifeldWertField;
            }
            set
            {
                this.cFreifeldWertField = value;
            }
        }
        /// <remarks/>
        public decimal fAufpreis
        {
            get
            {
                return this.fAufpreisField;
            }
            set
            {
                this.fAufpreisField = value;
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string[] Text
        {
            get
            {
                return this.textField;
            }
            set
            {
                this.textField = value;
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ulong kEigenschaft
        {
            get
            {
                return this.kEigenschaftField;
            }
            set
            {
                this.kEigenschaftField = value;
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool kEigenschaftSpecified
        {
            get
            {
                return this.kEigenschaftFieldSpecified;
            }
            set
            {
                this.kEigenschaftFieldSpecified = value;
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ulong kEigenschaftWert
        {
            get
            {
                return this.kEigenschaftWertField;
            }
            set
            {
                this.kEigenschaftWertField = value;
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool kEigenschaftWertSpecified
        {
            get
            {
                return this.kEigenschaftWertFieldSpecified;
            }
            set
            {
                this.kEigenschaftWertFieldSpecified = value;
            }
        }
    }
}
