﻿using BestellImport.DnOrderFormat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BestellImport.JTL_Import
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234")]
    [System.SerializableAttribute()]
 //   [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class tWarenkorbPos
    {
        private string cNameField;
        private string cArtNrField;
        private string cBarcodeField;
        private string cSeriennummerField;
        private string cEinheitField;
        private decimal fPreisEinzelNettoField;
        private decimal fPreisField;
        private decimal fMwStField;
        private decimal fAnzahlField;
        private postype cPosTypField;
        private decimal fRabattField;
        private tWarenkorbPosEigenschaft[] twarenkorbposeigenschaftField;
        private ulong kArtikelField;
        private bool kArtikelFieldSpecified;



        public tWarenkorbPos()
        {
        }


        public tWarenkorbPos(IOrderItem orderItem)
        {
            cName = orderItem.getTitle().Replace("'", "''"); 
            cArtNr= orderItem.getSKU();
            cBarcode = orderItem.getSKU();
            fPreis = orderItem.getPrice();
            fMwSt = orderItem.getTax();
            if (fMwSt == 0)
            {
 
            }
            fAnzahl = orderItem.getQuantity();
            cPosTyp = postype.standard;
            fPreisEinzelNetto = orderItem.getPrice() / ((100m + orderItem.getTax()) / 100);
            
        }

        /// <remarks/>
        public string cName
        {
            get
            {
                return this.cNameField;
            }
            set
            {
                this.cNameField = value;
            }
        }
        /// <remarks/>
        public string cArtNr
        {
            get
            {
                return this.cArtNrField;
            }
            set
            {
                this.cArtNrField = value;
            }
        }
        /// <remarks/>
        public string cBarcode
        {
            get
            {
                return this.cBarcodeField;
            }
            set
            {
                this.cBarcodeField = value;
            }
        }
        /// <remarks/>
        public string cSeriennummer
        {
            get
            {
                return this.cSeriennummerField;
            }
            set
            {
                this.cSeriennummerField = value;
            }
        }
        /// <remarks/>
        public string cEinheit
        {
            get
            {
                return this.cEinheitField;
            }
            set
            {
                this.cEinheitField = value;
            }
        }
        /// <remarks/>
        public decimal fPreisEinzelNetto
        {
            get
            {
                return this.fPreisEinzelNettoField;
            }
            set
            {
                this.fPreisEinzelNettoField = value;
            }
        }
        /// <remarks/>
        public decimal fPreis
        {
            get
            {
                return this.fPreisField;
            }
            set
            {
                this.fPreisField = value;
            }
        }
        /// <remarks/>
        public decimal fMwSt
        {
            get
            {
                return this.fMwStField;
            }
            set
            {
                this.fMwStField = value;
            }
        }
        /// <remarks/>
        public decimal fAnzahl
        {
            get
            {
                return this.fAnzahlField;
            }
            set
            {
                this.fAnzahlField = value;
            }
        }
        /// <remarks/>
        public postype cPosTyp
        {
            get
            {
                return this.cPosTypField;
            }
            set
            {
                this.cPosTypField = value;
            }
        }
        /// <remarks/>
        public decimal fRabatt
        {
            get
            {
                return this.fRabattField;
            }
            set
            {
                this.fRabattField = value;
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("twarenkorbposeigenschaft")]
        public tWarenkorbPosEigenschaft[] twarenkorbposeigenschaft
        {
            get
            {
                return this.twarenkorbposeigenschaftField;
            }
            set
            {
                this.twarenkorbposeigenschaftField = value;
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ulong kArtikel
        {
            get
            {
                return this.kArtikelField;
            }
            set
            {
                this.kArtikelField = value;
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool kArtikelSpecified
        {
            get
            {
                return this.kArtikelFieldSpecified;
            }
            set
            {
                this.kArtikelFieldSpecified = value;
            }
        }
    }
}
