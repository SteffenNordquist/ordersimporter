﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BestellImport.JTL_Import
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public enum waehrung
    {
        /// <remarks/>
        EUR,
        /// <remarks/>
        USD,
        /// <remarks/>
        GBP,
        /// <remarks/>
        AUD,
        /// <remarks/>
        CHF,
        /// <remarks/>
        CNY,
        /// <remarks/>
        PLN,
        /// <remarks/>
        SEK,
        /// <remarks/>
        SGD,
        /// <remarks/>
        TWD,
        /// <remarks/>
        INR,
        /// <remarks/>
        MYR,
    }
}
