﻿using BestellImport.DnOrderFormat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BestellImport.JTL_Import
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234")]
    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class tLieferadresse
    {
        private string cAnredeField;
        private string cVornameField;
        private string cNachnameField;
        private string cTitelField;
        private string cFirmaField;
        private string cStrasseField;
        private string cAdressZusatzField;
        private string cPLZField;
        private string cOrtField;
        private string cBundeslandField;
        private string cLandField;
        private string cTelField;
        private string cMobilField;
        private string cFaxField;
        private string cMailField;
        private ulong kLieferadresseField;


        public tLieferadresse(Address address)
        {
            cAnredeField = "";
            cVornameField = "";
            cNachnameField = address.name; 
            cTitelField = "";

            cFirmaField = getCompany(address);
            cAdressZusatzField = getAddtionalAddressAttribute(address);
            cStrasseField = getStreet(address); 


            cPLZField = address.zipCode; 
            cOrtField = address.city;
            cBundeslandField = ""; 
            cLandField = address.country;
            cTelField = address.phone;
            cMobilField = address.phone;
            cFaxField = "";
            cMailField = "";

        }

        public tLieferadresse()
        {
        }

        internal string getCompany(Address address)
        {
            if (address.addressLines.Count() > 1)
            {
                return address.addressLines[0];
            }
            else return "";
        }

        internal string getStreet(Address address)
        {
            if (address.addressLines.Count() > 1)
            {
                return address.addressLines[1];
            }
            else return address.addressLines[0];
        }

        internal string getAddtionalAddressAttribute(Address address)
        {
            if (address.addressLines.Count() > 2)
            {
                return address.addressLines[2];
            }
            else return "";
        }

        /// <remarks/>
        public string cAnrede
        {
            get
            {
                return this.cAnredeField;
            }
            set
            {
                this.cAnredeField = value;
            }
        }
        /// <remarks/>
        public string cVorname
        {
            get
            {
                return this.cVornameField;
            }
            set
            {
                this.cVornameField = value;
            }
        }
        /// <remarks/>
        public string cNachname
        {
            get
            {
                return this.cNachnameField;
            }
            set
            {
                this.cNachnameField = value;
            }
        }
        /// <remarks/>
        public string cTitel
        {
            get
            {
                return this.cTitelField;
            }
            set
            {
                this.cTitelField = value;
            }
        }
        /// <remarks/>
        public string cFirma
        {
            get
            {
                return this.cFirmaField;
            }
            set
            {
                this.cFirmaField = value;
            }
        }
        /// <remarks/>
        public string cStrasse
        {
            get
            {
                return this.cStrasseField;
            }
            set
            {
                this.cStrasseField = value;
            }
        }
        /// <remarks/>
        public string cAdressZusatz
        {
            get
            {
                return this.cAdressZusatzField;
            }
            set
            {
                this.cAdressZusatzField = value;
            }
        }
        /// <remarks/>
        public string cPLZ
        {
            get
            {
                return this.cPLZField;
            }
            set
            {
                this.cPLZField = value;
            }
        }
        /// <remarks/>
        public string cOrt
        {
            get
            {
                return this.cOrtField;
            }
            set
            {
                this.cOrtField = value;
            }
        }
        /// <remarks/>
        public string cBundesland
        {
            get
            {
                return this.cBundeslandField;
            }
            set
            {
                this.cBundeslandField = value;
            }
        }
        /// <remarks/>
        public string cLand
        {
            get
            {
                return this.cLandField;
            }
            set
            {
                this.cLandField = value;
            }
        }
        /// <remarks/>
        public string cTel
        {
            get
            {
                return this.cTelField;
            }
            set
            {
                this.cTelField = value;
            }
        }
        /// <remarks/>
        public string cMobil
        {
            get
            {
                return this.cMobilField;
            }
            set
            {
                this.cMobilField = value;
            }
        }
        /// <remarks/>
        public string cFax
        {
            get
            {
                return this.cFaxField;
            }
            set
            {
                this.cFaxField = value;
            }
        }
        /// <remarks/>
        public string cMail
        {
            get
            {
                return this.cMailField;
            }
            set
            {
                this.cMailField = value;
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ulong kLieferadresse
        {
            get
            {
                return this.kLieferadresseField;
            }
            set
            {
                this.kLieferadresseField = value;
            }
        }
    }
}
