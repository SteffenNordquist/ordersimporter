﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BestellImport
{
    public class AgentMapping : Dictionary<string,string>
    {
        public AgentMapping()
        {
            Add("9", "8");
            Add("16", "14"); // agentIdMappingOldServer
            Add("12", "2");
            Add("14", "7");
            Add("8", "5");
            Add("13", "11");
            Add("17", "15");
        }
    
    }
}
