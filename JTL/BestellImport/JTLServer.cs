﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BestellImport
{
    public class SQLServer
    {

        private string DatabaseServer;
        private string DatabaseUsername;
        private string DatabasePassword;
        private string DatabaseName;
        SqlConnection conn = new SqlConnection();

        public SQLServer(string databaseServer, string databaseUsername, string databasePassword, string databaseName)
        {
            this.DatabaseName = databaseName;
            this.DatabasePassword = databasePassword;
            this.DatabaseServer = databaseServer;
            this.DatabaseUsername = databaseUsername;
            
            
        }

        public DataTable SelectCommand(string SQL)
        {
            conn.ConnectionString = "Data Source=" + DatabaseServer + "; Initial Catalog=" + DatabaseName + ";User ID=" + DatabaseUsername + ";Password=" + DatabasePassword;
            SqlCommand myCommand = new SqlCommand();
            SqlDataAdapter myAdapter = new SqlDataAdapter();
            DataTable myData = new DataTable();
            myCommand.Connection = conn;
            myCommand.CommandText = SQL;
            myAdapter.SelectCommand = myCommand;
            myAdapter.Fill(myData);
            conn.Close();
            conn.Dispose();
            myAdapter.Dispose();
            myCommand.Dispose();
            return myData;
        }


        public void ExecuteCommand(string SQL)
        {
            try
            {
                conn.ConnectionString = "Data Source=" + DatabaseServer + "; Initial Catalog=" + DatabaseName + ";User ID=" + DatabaseUsername + ";Password=" + DatabasePassword;

                conn.Open();
                SqlCommand myCommand = new SqlCommand(SQL);
                myCommand.Connection = conn;
                myCommand.ExecuteNonQuery();

                conn.Close();
                conn.Dispose();

                myCommand.Dispose();
            }
            catch
            {
                throw;
            }
            finally {
                conn.Close();
            }
        }


        /// <summary>
        /// @M
        /// </summary>
        /// <param name="SQL"></param>
        /// <param name="parameter"></param>
        public void ExecuteCommand(string SQL, string parameter)
        {
            try
            {
                conn.ConnectionString = "Data Source=" + DatabaseServer + "; Initial Catalog=" + DatabaseName + ";User ID=" + DatabaseUsername + ";Password=" + DatabasePassword;

                conn.Open();
                SqlCommand myCommand = new SqlCommand(SQL);
                myCommand.Connection = conn;
                myCommand.Parameters.AddWithValue("@M", parameter); 
                myCommand.ExecuteNonQuery();

                conn.Close();
                conn.Dispose();

                myCommand.Dispose();
            }
            catch
            {
                throw;
            }
            finally
            {
                conn.Close();
            }
        }

        public DataTable Select2(string sql)
        {
            string conSTR = "Data Source=" + DatabaseServer + "; Initial Catalog=" + DatabaseName + ";User ID=" + DatabaseUsername + ";Password=" + DatabasePassword;

            using (SqlConnection sqlConn = new SqlConnection(conSTR))
            using (SqlCommand cmd = new SqlCommand(sql, sqlConn))
            {
                sqlConn.Open();
                DataTable dt = new DataTable();
                dt.Load(cmd.ExecuteReader());
                return dt;
            }
        }
    }
}
