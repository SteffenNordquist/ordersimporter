﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BestellImport.DnOrderFormat
{
    public class Address
    {


        /// <summary>
        /// ISO -> DE, US
        /// </summary>
        public string country { get; set; }

        public string zipCode { get; set; } // german PLZ, Postleitzahl
        public List<string> addressLines = new List<string>();
       
        public string city { get; set; }

        public string name{get; set; }
        public string phone { get; set; }

        

        public Address()
        { }

        public Address(BsonDocument address)
        {

            this.name = address["Name"].AsBsonValue.ToString().Replace("'", "");
            try
            {
                this.zipCode = address["PostalCode"].AsBsonValue.ToString();
            }
            catch { }

            this.city = address["City"].AsBsonValue.ToString().Replace("'", "");
            this.country = address["CountryCode"].AsBsonValue.ToString().Replace("'", "");
            try
            {
                this.phone = address["Phone"].AsBsonValue.ToString().Replace("'", "");
            }
            catch { }


            for (int i = 1; i <4; i++)
            {
                try {
                    string addressLine = address["AddressLine" + i].AsBsonValue.ToString().Replace("'", "");
                   
                        addressLines.Add(addressLine); 
                }
                catch
                {}
            }



        }

        public Address(EbayOrdersEntity shippingDetail)
        {
            this.name = shippingDetail.name.Replace("'", "");
            this.country = shippingDetail.country.Replace("'", "");
            this.phone = shippingDetail.phone.Replace("'", "");
            this.addressLines = shippingDetail.streets.Select(x => x.Replace("'", "")).ToList();
            this.zipCode = shippingDetail.postalcode.Replace("'", "");
        }

    }
}
