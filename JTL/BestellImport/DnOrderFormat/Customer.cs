﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BestellImport.DnOrderFormat
{
    public class Customer
    {
       
        public InvoiceAddress invoiceAddress{ get; set; }
        public DateTime? birthday{get; set;}
        public bool newsLetter{get;set;}
        public string email{get; set;}


        public Customer(BsonDocument order)
        {
            invoiceAddress = new InvoiceAddress(order["ShippingAddress"].AsBsonDocument);
            newsLetter = false;

            email = order["BuyerEmail"].ToString();
        }

        public Customer(EbayOrdersEntity ebay)
        {

            invoiceAddress = new InvoiceAddress(ebay);
            this.email = ebay.buyerEmail;
        }


        internal string getZipCode()
        {
            return invoiceAddress.zipCode;
        }

        internal string getCompany()
        {
            if (invoiceAddress.addressLines.Count() > 1)
            {
                return invoiceAddress.addressLines[0];
            }
            else return ""; 
        }

        internal string getStreet()
        {
            if (invoiceAddress.addressLines.Count() > 1)
            {
                return invoiceAddress.addressLines[1];
            }
            else return invoiceAddress.addressLines[0];
        }

        internal string getAddtionalAddressAttribute()
        {
            if (invoiceAddress.addressLines.Count() > 2)
            {
                return invoiceAddress.addressLines[2];
            }
            else return ""; 
        }

        internal string getPhoneNumber()
        {
            return invoiceAddress.phone;
        }

        internal string getEmail()
        {
            return email;
        }

        internal string getCity()
        {
            return invoiceAddress.city; 
        }
    }
}
