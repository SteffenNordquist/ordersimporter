﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BestellImport.DnOrderFormat.PlatformSpecific.Ebay
{
    public class EbayOrder :Order,IOrder
    {
   

        List<IOrderItem> ebayOrderItems = new List<IOrderItem>();


        public EbayOrder(BsonDocument bson)
        {

            EbayOrdersEntity ebay = new EbayOrdersEntity(bson);


            this.orderId = ebay.orderId;
            this.orderDate = stringToDateTime(ebay.orderDateString);
            this.lastUpdated = stringToDateTime(ebay.lastUpdatedString);
            this.orderStatus = IdentifyOrderStatus(ebay.orderStatus);
            this.paymentMethod = IdentifyPaymentMethod(ebay.paymentMethod);
            this.amountPaid = ebay.amountPaid;

            if (this.orderStatus != OrderStatus.cancelled)
            {
                try
                {
                    string buyerName = ebay.name;
                }
                catch
                { }

                this.totalAmount = ebay.total;

                this.customer = new Customer(ebay);
                this.shippingAddress = new Address(ebay);


                var items = ebay.itemDetail;
                    foreach (var item in items)
                    {
                        
                        EbayOrderItem ebayOrderItem = new EbayOrderItem(item);
                        ebayOrderItems.Add(ebayOrderItem); 
                    }

                        shippingIncome = getShippingIncome();
                   
                //if( bson["Order"]["Address"].GetType() == BsonArray)
                //{
                //}

            }

        }

        private string IdentifyPaymentMethod(string paymentMethod)
        {
            if (paymentMethod == "0")
            {
                return PaymentMethod.None.ToString().ToString();
            }
            else if (paymentMethod == "1")
            {

                return PaymentMethod.MOCC.ToString();
            }
            else if (paymentMethod == "2")
            {

                return PaymentMethod.AmEx.ToString();
            }
            else if (paymentMethod == "3")
            {

                return PaymentMethod.PaymentSeeDescription.ToString();
            }
            else if (paymentMethod == "4")
            {

                return PaymentMethod.CCAccepted.ToString();
            }
            else if (paymentMethod == "5")
            {

                return PaymentMethod.PersonalCheck.ToString();
            }
            else if (paymentMethod == "6")
            {

                return PaymentMethod.COD.ToString();
            }
            else if (paymentMethod == "7")
            {

                return PaymentMethod.VisaMC.ToString();
            }
            else if (paymentMethod == "8")
            {

                return PaymentMethod.PaisaPayAccepted.ToString();
            }
            else if (paymentMethod == "9")
            {

                return PaymentMethod.Other.ToString();
            }
            else if (paymentMethod == "10")
            {

                return PaymentMethod.PayPal.ToString();
            }
            else if (paymentMethod == "11")
            {

                return PaymentMethod.Discover.ToString();
            }
            else if (paymentMethod == "12")
            {

                return PaymentMethod.CashOnPickup.ToString();
            }
            else if (paymentMethod == "13")
            {

                return PaymentMethod.MoneyXferAccepted.ToString();
            }
            else if (paymentMethod == "14")
            {

                return PaymentMethod.MoneyXferAcceptedInCheckout.ToString();
            }
            else if (paymentMethod == "15")
            {

                return PaymentMethod.OtherOnlinePayments.ToString();
            }
            else if (paymentMethod == "16")
            {

                return PaymentMethod.Escrow.ToString();
            }
            else if (paymentMethod == "17")
            {

                return PaymentMethod.PrePayDelivery.ToString();
            }
            else if (paymentMethod == "18")
            {

                return PaymentMethod.CODPrePayDelivery.ToString();
            }
            else if (paymentMethod == "19")
            {

                return PaymentMethod.PostalTransfer.ToString();
            }
            else if (paymentMethod == "20")
            {

                return PaymentMethod.CustomCode.ToString();
            }
            else if (paymentMethod == "21")
            {

                return PaymentMethod.LoanCheck.ToString();
            }
            else if (paymentMethod == "22")
            {

                return PaymentMethod.CashInPerson.ToString();
            }
            else if (paymentMethod == "23")
            {

                return PaymentMethod.ELV.ToString();
            }
            else if (paymentMethod == "24")
            {

                return PaymentMethod.PaisaPayEscrow.ToString();
            }
            else
            {
                return PaymentMethod.PaisaPayEscrowEMI.ToString();
            }
        }

        private OrderStatus IdentifyOrderStatus(int orderStatus)
        {
            if (orderStatus == 0)
            {
                return OrderStatus.active;
            }
            else if (orderStatus == 1)
            {

                return OrderStatus.inactive;
            }
            else if (orderStatus == 2)
            {

                return OrderStatus.completed;
            }
            else if (orderStatus == 3)
            {

                return OrderStatus.cancelled;
            }
            else if (orderStatus == 4)
            {

                return OrderStatus.shipped;
            }
            else if (orderStatus == 5)
            {

                return OrderStatus._default;
            }
            else if (orderStatus == 6)
            {

                return OrderStatus.authenticated;
            }
            else if (orderStatus == 7)
            {

                return OrderStatus.inprocess;
            }
            else if (orderStatus == 8)
            {

                return OrderStatus.invalid;
            }
            else if (orderStatus == 9)
            {

                return OrderStatus.customcode;
            }
            else if (orderStatus == 10)
            {

                return OrderStatus.all;
            }
            else 
            {
                return OrderStatus.pending;
            }

        }

        public double getShippingIncome()
        {
            return ebayOrderItems.Select(x => x.getShippingIncome()).Max();
            
        } 

        private DateTime stringToDateTime(string dateString)
        {

            if (dateString.Contains("."))
            {
                return DateTime.ParseExact(dateString, "dd.MM.yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            }
            else if (dateString.Contains("/"))
            {
                return DateTime.ParseExact(dateString, "M/d/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);
            }
            else {

                string date = dateString.Replace("T", " ").Replace("Z", "");

                return DateTime.ParseExact(date, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
               
            
            }
            //yyyy-MM-dd HH:mm:ss.FFFK
        }
        public string getOrderId()
        {
            return orderId;
        }

        public double getTotalAmount()
        {
            return totalAmount;
        }


        public string getFirstName()
        {
            return "";
        }

        public string getLastName()
        {
            return shippingAddress.name;
        }


        public List<IOrderItem> getOrderItmes()
        {
            return ebayOrderItems;
        }

        public Customer getCustomer()
        {
            return customer;
        }

        public DateTime getOrderDate()
        {
            return orderDate;
        }


        public OrderStatus getOrderStatus()
        {
            return orderStatus;
        }


        public double getAmountPaid()
        {
            return amountPaid;
        }


        public string getPaymentMethod()
        {
            return paymentMethod;
        }


        public string getCountry()
        {
            throw new NotImplementedException();
        }


        public Address getShippingAddress()
        {
            throw new NotImplementedException();
        }


        public string getAgentId()
        {
            throw new NotImplementedException();
        }

        public string getExternalCompanyId()
        {
            throw new NotImplementedException();
        }


        public string GetPlatformName()
        {
            return "Ebay";
        }
    }
}
