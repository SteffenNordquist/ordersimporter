﻿using MongoDB.Bson;
using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BestellImport.DnOrderFormat
{
    public class EbayOrdersEntity
    {

            public string orderId { get; set; }
            public string orderDateString { get; set; }
            public string lastUpdatedString { get; set; }
            public int orderStatus { get; set; }
            public double total { get; set; }
            public string paymentMethod { get; set; } 
            public double amountPaid{get;set;}
       
            public string name { get; set; }
            public string city { get; set; }
            public string country { get; set; }
            public string postalcode { get; set; }
            public string phone { get; set; }
            public List<string> streets { get; set; }
      
            public string buyerId { get; set; }
            public string buyerEmail { get; set; }

            public List<EbayOrdersEntity.ItemDetail> itemDetail { get; set; }
            public BsonDocument plus { get; set; }

            public class ItemDetail
            {
            public string sku { get; set; }
            public int qtyPurchased { get; set; }
            public string title { get; set; }
            public double itemprice { get; set; }
            public double shippingIncome { get; set; }

            }


        public EbayOrdersEntity(BsonDocument bson) {

            this.orderId = bson["order"]["OrderID"].AsBsonValue.ToString();
            this.orderDateString = bson["order"]["CreatedTime"].AsBsonValue.ToString();
            this.lastUpdatedString = bson["order"]["CheckoutStatus"]["LastModifiedTime"].AsBsonValue.ToString();
            this.orderStatus = bson["order"]["OrderStatus"].AsBsonValue.ToInt32();
            this.total = double.Parse(bson["order"]["Total"]["Value"].AsBsonValue.ToString(), CultureInfo.InvariantCulture);
            this.amountPaid = double.Parse(bson["order"]["AmountPaid"]["Value"].AsBsonValue.ToString(), CultureInfo.InvariantCulture);
            this.paymentMethod = bson["order"]["CheckoutStatus"]["PaymentMethod"].AsBsonValue.ToString();

            this.name = bson["order"]["ShippingAddress"]["Name"].AsBsonValue.ToString();
            this.postalcode = bson["order"]["ShippingAddress"]["PostalCode"].AsBsonValue.ToString();
            this.city = bson["order"]["ShippingAddress"]["CityName"].AsBsonValue.ToString();
            this.country = bson["order"]["ShippingAddress"]["CountryName"].AsBsonValue.ToString();
            string phonenum = bson["order"]["ShippingAddress"]["Phone"].AsBsonValue.ToString();

            if(phonenum.Contains("Invalid")){this.phone = "";}else{this.phone=phonenum;}
            this.streets = GetStreets(bson["order"]["ShippingAddress"].AsBsonDocument);

            List<EbayOrdersEntity.ItemDetail> itemDetailList = new List<EbayOrdersEntity.ItemDetail>();
            itemDetailList = GetItems(bson["order"]["TransactionArray"].AsBsonArray);
            this.itemDetail = itemDetailList;

            this.buyerId = bson["order"]["BuyerUserID"].AsBsonValue.ToString();
            this.buyerEmail = bson["order"]["TransactionArray"][0]["Buyer"]["Email"].AsBsonValue.ToString();

            
        }

        private List<EbayOrdersEntity.ItemDetail> GetItems(BsonArray trans)
        {
            List<EbayOrdersEntity.ItemDetail> items = new List<EbayOrdersEntity.ItemDetail>();
			double shippingIncome = 3; // Default 3 in the process profit. Ask Steffen
            foreach (var a in trans)
            {
                EbayOrdersEntity.ItemDetail item = new EbayOrdersEntity.ItemDetail()
                {
                    sku = a["Item"]["SKU"].ToString(),
                    qtyPurchased = a["QuantityPurchased"].ToInt32(),
                    title = a["Item"]["Title"].ToString(),
					itemprice = Math.Round(Convert.ToDouble(a["TransactionPrice"]["Value"]), 2),
					shippingIncome = Math.Round(shippingIncome,2)

                };

                items.Add(item);
            
            }
            return items;
        }


        private List<string> GetStreets(BsonDocument address)
        {
            List<string> streets = new List<string>();
            for (int i = 1; i < 4; i++)
            {
                try
                {
                    string street = address["Street" + i].AsBsonValue.ToString();
                    streets.Add(street);
                }
                catch
                { }
            }

            return streets;
        }



    }

   
   
}
