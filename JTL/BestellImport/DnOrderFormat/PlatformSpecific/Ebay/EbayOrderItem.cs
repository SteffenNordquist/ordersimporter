﻿using MongoDB.Bson;
using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BestellImport.DnOrderFormat.PlatformSpecific.Ebay
{
    public class EbayOrderItem : OrderItem, IOrderItem
    {
        private Regex _skuRegex = null;
        private int QuantiyFromMultiplier
        {
            get
            {
                return getMultiplierFromSKU();
            }
        }

        public string SKU = "";
     


        public EbayOrderItem(EbayOrdersEntity.ItemDetail item)
        {
        

            this.SKU = item.sku;
            
            this.title = item.title;
            this.quantity = item.qtyPurchased;
            price = item.itemprice;
            shippingIncome = item.shippingIncome;

            ProductFinder productFinder = new ProductFinder();
            try
            {
                tax = (double)productFinder.FindAllProductsByEan(filteredSKU, new List<string>()).First().getTax();
                Console.WriteLine(tax);
            }
            catch
            {
                tax = 19.0;
                Console.WriteLine("TaxError:19");
            }

            shippingIncomeTax = tax;
            this.pricenet = Math.Round(item.itemprice / ((100.0 + shippingIncomeTax) / 100),2);
        }

        private int getMultiplierFromSKU()
        {
            Regex regex = new Regex(@"\b-\d{1,4}\b");
            var multiplierString = regex.Match(rawSKU).Value.Trim('-');

            int multiplier = 1;
            int.TryParse(multiplierString, out multiplier);

            return Math.Max(multiplier, 1);
        }

        public double getShippingIncome()
        {
            return Math.Round(shippingIncome * ((100.0 + shippingIncomeTax) / 100), 2);
            
        }

        public string getSKU()
        {
            return filteredSKU;
        }


        public string getTitle()
        {
            return title;
        }

        public decimal getPrice()
        {
            return (decimal)price;
        }

        public decimal getTax()
        {
            return (decimal)tax;
        }

        public decimal getQuantity()
        {
            return quantity;
        }
    }
}
