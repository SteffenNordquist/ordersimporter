﻿using BestellImport.DnOrderFormat.Interfaces;
using BestellImport.JTL_Import;
using DN_Classes.QueriesCommands;
using DN_Classes.QueriesCommands.Orders;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BestellImport.DnOrderFormat.PlatformSpecific.Ebay
{
   public class Ebay: IPlatform
    {
       public List<IOrder> ebayOrderList { get; set; }
       private List<BsonDocument> ebayOrders { get; set; }

       public Ebay(List<BsonDocument> ebayOrders)
       {

           this.ebayOrders = ebayOrders;
           this.ebayOrderList = GetOrderList();
       
       }

        public List<IOrder> GetOrderList()
        {
            List<IOrder> ebayOrderList = new List<IOrder>();
            foreach (var orderBson in this.ebayOrders)
            {
                var amazonOrder = new EbayOrder(orderBson);
                ebayOrderList.Add(amazonOrder);
            }

            return ebayOrderList;
        }

        public void CreateXML()
        {
            new WriteXML(this.ebayOrderList, XMLPath());
        }

        public string SkuRegex()
        {
            return "";
        }


        public string XMLPath()
        {
           return "C:\\temp\\0_second_2.xml";
        }


         /// <summary>
        /// checks if product is already in the JTL, so we don't have duplicates
        /// </summary>
        /// <param name="jtlVersion"></param>
        /// <param name="sqlServer"></param>
        /// <returns></returns>
        public void removeImported(string jtlVersion, SQLServer sqlServer)
        {
            List<IOrder> alreadyInJTL = new List<IOrder>(); 
            foreach (var order in ebayOrderList)
            {
                var dataTable = sqlServer.SelectCommand(string.Format("select * from [tbestellung] where [cInetBestellNr] ='{0}'", order.getOrderId()));
                if (dataTable.Rows.Count > 0)
                {
                    alreadyInJTL.Add(order); 
                }
            }

            foreach (var remove in alreadyInJTL)
            {
                ebayOrderList.Remove(remove);
                setImported(jtlVersion,remove);
            }

            //set Cancelled as imported 
        }


        private void setImported(string jtlVersion, IOrder setImported)
        {
            OrdersQueries orderQueries = new OrdersQueries();
            orderQueries.setImported(setImported.getOrderId(), jtlVersion);
        }
    }
}

