﻿using BestellImport.DnOrderFormat.Interfaces;
using BestellImport.JTL_Import;
using DN_Classes.QueriesCommands;
using DN_Classes.QueriesCommands.Orders;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BestellImport.DnOrderFormat.PlatformSpecific.Amazon
{
    public class Amazon : IPlatform
    {
        public List<IOrder> amazonOrderList { get; set; }
        private List<BsonDocument> amazonOrders { get; set; }

        private DateTime _startDate;

        public Amazon(List<BsonDocument> amazonOrders, DateTime startDate)
        {
            _startDate = startDate;

            this.amazonOrders = amazonOrders;


            this.amazonOrderList = GetOrderList();
        }

        public List<IOrder> GetOrderList()
        {
            List<IOrder> amaOrderList = new List<IOrder>();
            string sku = SkuRegex();
            foreach (var orderBson in this.amazonOrders)
            {
                IOrder amazonOrder = null;
                try
                {
                    amazonOrder = new AmazonOrder(orderBson, sku, _startDate);
                    amaOrderList.Add(amazonOrder);
                }
                catch { }
                
            }
            return amaOrderList;
        }

        public void CreateXML()
        {
            new WriteXML(this.amazonOrderList, XMLPath());
        }

        public string SkuRegex()
        {
            return @"(\b\w\d{1,3}-.*){1}|(\d{13}){1}";
        }

        public string XMLPath()
        {
            return string.Format("C:\\temp\\Amazon{0}.xml", DateTime.Now.Ticks);
        }




        /// <summary>
        /// checks if product is already in the JTL, so we don't have duplicates
        /// </summary>
        /// <param name="jtlVersion"></param>
        /// <param name="sqlServer"></param>
        /// <returns></returns>
        public void removeImported(string jtlVersion, SQLServer sqlServer)
        {
            List<IOrder> alreadyInJTL = new List<IOrder>();
            foreach (var order in amazonOrderList)
            {
                var amazonOrderId = order.getOrderId();
                var dataTable = sqlServer.SelectCommand(string.Format("select * from [tbestellung] where [cInetBestellNr] ='{0}'", amazonOrderId));
                if (dataTable.Rows.Count > 0)
                {
                    alreadyInJTL.Add(order);
                }
            }

            foreach (var remove in alreadyInJTL)
            {
                amazonOrderList.Remove(remove);
                setImported(jtlVersion, remove);
            }

            //set Cancelled as imported 
        }


        private void setImported(string jtlVersion, IOrder setImported)
        {
            OrdersQueries orderQueries = new OrdersQueries();
            orderQueries.setImported(setImported.getOrderId(), jtlVersion);
        }

        public List<string> CreateXmlList()
        {
            var xmlWriter = new WriteXML();
            return xmlWriter.write(amazonOrderList); 
        }

    }
}
