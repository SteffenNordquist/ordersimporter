﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;

namespace BestellImport.DnOrderFormat.PlatformSpecific
{
    public class AmazonOrder : Order, IOrder
    {
        public string fulfillmentChannel = "";

        List<IOrderItem> amazonOrderItems = new List<IOrderItem>();

        private DateTime _startDate; 

        public double getShippingIncome () {
            return amazonOrderItems.Sum(x => x.getShippingIncome()); 
        } 


        public AmazonOrder(BsonDocument bson, string skuRegex, DateTime startDate)
        {
            _startDate = startDate;
            this.orderId = bson["Order"]["AmazonOrderId"].AsBsonValue.ToString();

            string orderDateString = bson["Order"]["PurchaseDate"].AsBsonValue.ToString();
            this.orderDate = stringToDateTime(orderDateString);

            string lastUpdatedString = bson["Order"]["LastUpdateDate"].AsBsonValue.ToString();
            this.lastUpdated = stringToDateTime(bson["Order"]["LastUpdateDate"].AsBsonValue.ToString());

            this.earliestShipDate = stringToDateTime(bson["Order"]["EarliestShipDate"].AsBsonValue.ToString());
            this.latestShipDate = stringToDateTime(bson["Order"]["LatestShipDate"].AsBsonValue.ToString());
            this.amountPaid = 0.0;

            string companyId = ""; 
            try
            {
                agentId = bson["Order"]["AgentId"].AsBsonValue.ToString();
                companyId = Program.agentMapping[agentId];
            }catch
            {}


            externalCompanyId = companyId; 
            
            
            fulfillmentChannel = bson["Order"]["FulfillmentChannel"].AsBsonValue.ToString();

            string orderStatus = bson["Order"]["OrderStatus"].AsBsonValue.ToString();

            if (orderStatus == "Shipped")
            {
                this.orderStatus = OrderStatus.shipped;
            }
            else if (orderStatus == "Canceled")
            {
                this.orderStatus = OrderStatus.cancelled;
            }
            else if (orderStatus == "Unshipped")
            {
                this.orderStatus = OrderStatus.ordered;
            }
            else if (orderStatus == "Pending")
            {
                this.orderStatus = OrderStatus.pending;
            }
            else
            {
                this.orderStatus = OrderStatus.undefined; 
            }



            if (this.orderStatus != OrderStatus.cancelled && this.orderStatus != OrderStatus.pending)
            {
                try
                {
                    string buyerName = bson["Order"]["BuyerName"].AsBsonValue.ToString().Replace("'", ""); 
                    this.paymentMethod = bson["Order"]["PaymentMethod"].AsBsonValue.ToString();
                }
                catch
                { }

                this.totalAmount = double.Parse(bson["Order"]["OrderTotal"]["Amount"].AsBsonValue.ToString(), CultureInfo.InvariantCulture);

                customer = new Customer(bson["Order"].AsBsonDocument);
                shippingAddress = new Address(bson["Order"]["ShippingAddress"].AsBsonDocument);

                var type = bson["Order"]["ASIN"].GetType();

                if (orderDate > startDate)
                {
                    if (type == typeof(BsonString))
                    {
                        AmazonOrderItem amazonOrderItem = new AmazonOrderItem(bson["Order"].AsBsonDocument, shippingAddress.country,skuRegex);
                        amazonOrderItems.Add(amazonOrderItem);
                    }
                    else
                    {
                        var bsonArray = bson["Order"]["ASIN"].AsBsonArray;
                        foreach (var a in bsonArray)
                        {
                            var index = bsonArray.IndexOf(a);
                            AmazonOrderItem amazonOrderItem = new AmazonOrderItem(bson["Order"].AsBsonDocument, shippingAddress.country,skuRegex, index);
                            amazonOrderItems.Add(amazonOrderItem);
                        }
                    }

                }
                else
                {
                    Console.WriteLine("Order not in correct Date range"); 
                }


                shippingIncome = getShippingIncome(); 

                //if( bson["Order"]["Address"].GetType() == BsonArray)
                //{
                //}

            }

        }

        private DateTime stringToDateTime(string dateString)
        {

            if (dateString.Contains("."))
            {
                return DateTime.ParseExact(dateString, "dd.MM.yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            }
            else
            {
                if(dateString.Contains("AM") || dateString.Contains("PM"))
                {
                    return DateTime.ParseExact(dateString, "M/d/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);
                }
                else
                {
                    return DateTime.ParseExact(dateString, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                }
            }
        }

        public string getOrderId()
        {
            return orderId;
        }

        public double getTotalAmount()
        {
            return totalAmount;
        }


        public string getFirstName()
        {
            return ""; 
        }

        public string getLastName()
        {
            return shippingAddress.name;
        }


        public List<IOrderItem> getOrderItmes()
        {
            return amazonOrderItems;
        }

        public Customer getCustomer()
        {
            return customer; 
        }

        public DateTime getOrderDate()
        {
            return orderDate;
        }


        public OrderStatus getOrderStatus()
        {
            return orderStatus; 
        }

        public double getAmountPaid()
        {
            return 0.0;
        }

        public string getPaymentMethod()
        {
            return "Amazon Payment";
        }


        public string getCountry()
        {
            return shippingAddress.country;
        }


        public Address getShippingAddress()
        {
            return shippingAddress; 
        }


        public string getAgentId()
        {
            return agentId;
        }

        public string getExternalCompanyId()
        {
            return externalCompanyId;
        }


        public string GetPlatformName()
        {
            return "Amazon"; 
        }
    }
}
