﻿using MongoDB.Bson;
using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BestellImport.DnOrderFormat
{
    public class AmazonOrderItem: OrderItem ,IOrderItem
    {
        private Regex _skuRegex = null;
        private int QuantiyFromMultiplier { get {
            return getMultiplierFromSKU();
        } }

        public string ASIN = "";
      


        /// <summary>
        /// (\b\w\d{1,3}-.*){1}|(\d{13}){1}
        /// </summary>
        /// <param name="orderItemBson"></param>
        /// <param name="skuRegex"></param>
        public AmazonOrderItem(BsonDocument orderItemBson, string countryCode, string skuRegex)
        {
            
            _skuRegex = new Regex(skuRegex);
            filteredSKU = _skuRegex.Match(rawSKU).Value;

            ASIN = orderItemBson["ASIN"].ToString();
            rawSKU = orderItemBson["SellerSKU"].ToString();
            title = orderItemBson["Title"].ToString();
            var quantityOrdered = int.Parse(orderItemBson["QuantityOrdered"].ToString());
            price = double.Parse(orderItemBson["ItemPrice"]["Amount"].ToString(), CultureInfo.InvariantCulture);
            // this is special to amazon. ["ItemPrice"]["Amount"] is already the total amount

            try
            {
                shippingIncome = double.Parse(orderItemBson["ShippingPrice"]["Amount"].ToString(), CultureInfo.InvariantCulture);
            }
            catch { }
            
            _skuRegex = new Regex(skuRegex);
            filteredSKU = _skuRegex.Match(rawSKU).Value;


            quantity = quantityOrdered * QuantiyFromMultiplier;

            price = price / quantity;

            ProductFinder productFinder = new ProductFinder();

            GetTaxForProduct(countryCode, productFinder);
        }

        private void GetTaxForProduct(string countryCode, ProductFinder productFinder)
        {
            if (isEUCounty(countryCode))
            {
                try
                {
                    tax = (double)productFinder.FindAllProductsByEan(filteredSKU, new List<string>()).First().getTax();

                    if (tax == 0)
                    {
                        Console.WriteLine("Tax 0, set to 19", filteredSKU);
                        tax = 19;
                    }
                    Console.WriteLine(tax);
                }
                catch
                {
                    tax = 19.0;
                    Console.WriteLine("TaxError:19");
                }
            }
            else
            {
                Console.WriteLine("Drittland");
                tax = 0.0;
            }
        }

        private bool isEUCounty(string countryCode)
        {
            return "BE;BG;DK;DE;EE;FI;FR;GR;IE;IT;HR;LV;LT;LU;MT;NL;AT;PL;PT;EO;SM;SE;SK;SI;ES;CZ;HU;GB;VA;FO;CY".Contains(countryCode);
        }

        public AmazonOrderItem(BsonDocument orderItemBson, string countryCode ,string skuRegex, int index)
        {

            ASIN = orderItemBson["ASIN"][index].ToString();
            rawSKU = orderItemBson["SellerSKU"][index].ToString();
            title = orderItemBson["Title"][index].ToString().Replace("'", "");
            var quantityOrdered = int.Parse(orderItemBson["QuantityOrdered"][index].ToString());
            price = double.Parse(orderItemBson["ItemPrice"][index]["Amount"].ToString(),CultureInfo.InvariantCulture);
            try
            {
                shippingIncome = double.Parse(orderItemBson["ShippingPrice"][index]["Amount"].ToString(), CultureInfo.InvariantCulture);

            }
            catch {
                shippingIncome = 0;
            }

            _skuRegex = new Regex(skuRegex);
            filteredSKU = _skuRegex.Match(rawSKU).Value;


            quantity = quantityOrdered * QuantiyFromMultiplier;


            ProductFinder productFinder = new ProductFinder();

            GetTaxForProduct(countryCode, productFinder);           

        }


        private int getMultiplierFromSKU()
        {
            Regex regex = new Regex(@"\b-\d{1,4}\b");
            var multiplierString = regex.Match(rawSKU).Value.Trim('-');

            int multiplier = 1;
            int.TryParse(multiplierString, out multiplier);

            return Math.Max(multiplier,1);
        }

        public double getShippingIncome()
        {
            return shippingIncome; 
        }

        public string getSKU()
        {
            return filteredSKU; 
        }


        public string getTitle()
        {
            return title; 
        }

        public decimal getPrice()
        {
            return (decimal)price;
        }

        public decimal getTax()
        {
            return (decimal) tax;
        }

        public decimal getQuantity()
        {
            return quantity;
        }
    }
}
