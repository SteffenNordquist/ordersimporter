﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BestellImport.DnOrderFormat
{
    public class OrderItem
    {
        public string title = ""; 
        public double price = 0.0;
        public double pricenet = 0.0;
        public double shippingIncome = 0.0;
        public double shippingIncomeTax = 0.0; 
        public double tax = 0.0;
        public string currency = "";

        public int quantity = 0;

        public string rawSKU = "";
        public string filteredSKU = "";
    }
}
