﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BestellImport.DnOrderFormat
{
    public enum PaymentMethod
    {
            None,
            MOCC,
            AmEx,
            PaymentSeeDescription,
            CCAccepted,
            PersonalCheck,
            COD,
            VisaMC,
            PaisaPayAccepted,
            Other,
            PayPal,
            Discover,
            CashOnPickup,
            MoneyXferAccepted,
            MoneyXferAcceptedInCheckout,
            OtherOnlinePayments,
            Escrow,
            PrePayDelivery,
            CODPrePayDelivery,
            PostalTransfer,
            CustomCode,
            LoanCheck,
            CashInPerson,
            ELV,
            PaisaPayEscrow,
            PaisaPayEscrowEMI
    }
}
