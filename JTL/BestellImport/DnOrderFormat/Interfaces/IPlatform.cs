﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BestellImport.DnOrderFormat.Interfaces
{
    public interface IPlatform
    {
        
        List<IOrder> GetOrderList();
        void CreateXML();
        string SkuRegex();
        string XMLPath();

      //void setImported(string jtlVersion, SQLServer sqlServer);
        void removeImported(string jtlVersion, SQLServer sqlServer);

    }
}
