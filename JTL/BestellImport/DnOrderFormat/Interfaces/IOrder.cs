﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BestellImport.DnOrderFormat
{
    public interface IOrder
    {
         string getOrderId();
         double getTotalAmount();
         double getShippingIncome();

         string getFirstName();
         string getLastName();
       
         double getAmountPaid();

        List<IOrderItem> getOrderItmes();
        Customer getCustomer();

        Address getShippingAddress(); 


        DateTime getOrderDate();
        string getPaymentMethod();
        OrderStatus getOrderStatus();

        string getCountry();

        string getAgentId();
        string getExternalCompanyId();



            //        tBestellung order = new tBestellung();

            //order.cBemerkung = "bem";
            //order.dErstellt = DateTime.Now;

            //order.cBestellNr = "bestellNr_12";
            //order.cKommentar= "kein Kommentar";
            //order.cLogistiker = "";
            //order.cSprache = sprache.ger;
            //order.cTracking = "";
            //order.cVersandartName = "DHL";
            //order.cVersandInfo = "";
            //order.cWaehrung = waehrung.EUR;
            //order.cZahlungsartName = "Amazon Zahlung";
            //order.dBezahltDatum  = "";
            //order.dErstellt = DateTime.Now;
            //order.dLieferDatum = "";
            //order.dVersandDatum = "";
            //order.fBezahlt = (decimal)0.0;
            //order.fGesamtsumme = (decimal)0.01;
            //order.fGuthaben = (decimal)0.0;
            ////order.kBenutzer = 

            //tKunde kunde = new tKunde(); 
            //kunde.cKundenNr = "testKunde 123";
            //kunde.cNachname = "Nachname";
            //kunde.cPLZ = "63512";
            //kunde.cStrasse= "Teststrasse 34"; 
            //kunde.cTel = "09876";
            //kunde.cTitel = "Herr"; 
            //kunde.cVorname= "Rez"; 
            //kunde.cWWW = "www.www.ww";
            //kunde.dErstellt = DateTime.Now;
            //kunde.cFirma = ""; 
            



            //order.tkunde = kunde;

            //List<tWarenkorbPos> orderItems = new List<tWarenkorbPos>();
            
            //tWarenkorbPos orderItem = new tWarenkorbPos();
            //orderItem.cBarcode = "";
            //orderItem.fPreis = (decimal)0.01;
            //orderItem.fMwSt = (decimal)19.0;
            //orderItem.cArtNr = "A4-9783121733200";
            //orderItem.fAnzahl = 1;
            ////orderItem.kArtikel = 352881;

            //orderItems.Add(orderItem);

            

            //order.twarenkorbpos = orderItems.ToArray();

            //order.cExterneBestellNr = "1234567890"; 



        /*private sprache cSpracheField;
        private waehrung cWaehrungField;
        private decimal fGuthabenField;
        private decimal fGesamtsummeField;
        private string cBestellNrField;
        private string cExterneBestellNrField; // getSKU
        private string cVersandartNameField;
        private string cVersandInfoField;
        private string dVersandDatumField;
        private string cTrackingField;
        private string cLogistikerField;
        private string dLieferDatumField;
        private string cKommentarField;
        private string cBemerkungField;
        private System.DateTime dErstelltField;
        private string cZahlungsartNameField;
        private string dBezahltDatumField;
        private decimal fBezahltField;
        private tWarenkorbPos[] twarenkorbposField;
        private tKunde tkundeField;
        private tLieferadresse tlieferadresseField;
        private tZahlungsInfo tzahlungsinfoField;
        private ulong kFirmaField;
        private bool kFirmaFieldSpecified;
        private ulong kBenutzerField;
        private bool kBenutzerFieldSpecified;
         * 
         * */



        string GetPlatformName();
    }


}
