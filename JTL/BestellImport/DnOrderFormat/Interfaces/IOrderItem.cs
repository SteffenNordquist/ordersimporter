﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BestellImport.DnOrderFormat
{
    public interface IOrderItem
    {
        string getSKU();

        double getShippingIncome();

        string getTitle();

        decimal getPrice();

        decimal getTax();

        decimal getQuantity();
    }
}
