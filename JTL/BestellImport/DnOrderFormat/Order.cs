﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BestellImport.DnOrderFormat
{
    public class Order
    {
        public string orderId { get; set; }
        public DateTime orderDate { get; set; }
        public DateTime lastUpdated { get; set; }

        public DateTime earliestShipDate { get; set; }
        public DateTime latestShipDate { get; set; }

        private Address _shippingAddress = null;
        public Customer customer { get; set; }

        public OrderStatus orderStatus { get; set; }
        public string paymentMethod { get; set; }
        public double amountPaid { get; set; }
        public double totalAmount { get; set; }
        public double shippingIncome { get; set; }

        public string agentId { get; set; }
        public string externalCompanyId { get; set;  }

        public Address shippingAddress
        {
            get
            {
                if (_shippingAddress == null)
                {
                    return customer.invoiceAddress;
                }
                else return _shippingAddress;
            }
            set
            {
                _shippingAddress = value;
            }
        }
    }
}
