﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BestellImport.DnOrderFormat
{
    public enum OrderStatus
    {
        ordered, 
        shipped, 
        cancelled, 
        undefined, 
        pending, 
        active,
        inactive,
        completed,
        _default,
        authenticated,
        inprocess,
        invalid,
        customcode,
        all
    }
}
