﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BestellImport.DnOrderFormat
{
    public class InvoiceAddress : Address
    {
        

        public InvoiceAddress()
        { }

        public InvoiceAddress(BsonDocument address)
        {

            this.name = address["Name"].AsBsonValue.ToString().Replace("'", ""); 
            try
            {
                this.zipCode = address["PostalCode"].AsBsonValue.ToString().Replace("'", "");
            }
            catch
            { }
            this.city = address["City"].AsBsonValue.ToString().Replace("'", "");
            this.country = address["CountryCode"].AsBsonValue.ToString();

            try
            {
                this.phone = address["Phone"].AsBsonValue.ToString().Replace("'", "");
            }
            catch { }


            for (int i = 1; i < 4; i++)
            {
                try
                {
                    string addressLine = address["AddressLine" + i].AsBsonValue.ToString().Replace("'", "");


                    addressLines.Add(addressLine.Replace("'", ""));
                }
                catch
                { }
            }


        }

        public InvoiceAddress(EbayOrdersEntity shippingDetail)
        {


            this.name = shippingDetail.name.Replace("'", "");
            this.zipCode = shippingDetail.postalcode.Replace("'", "");
            this.city = shippingDetail.city.Replace("'", "");
            this.country = shippingDetail.country.Replace("'", "");
            try
            {
                this.phone = shippingDetail.phone.Replace("'", "");
            }
            catch { }

            this.addressLines = shippingDetail.streets.Select(x => x.Replace("'", "")).ToList();
 
        }
    }
}
