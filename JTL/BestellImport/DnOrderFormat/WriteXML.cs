﻿using BestellImport.JTL_Import;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BestellImport.DnOrderFormat
{
    public class WriteXML
    {



        public WriteXML()
        {





        }


        public WriteXML(List<IOrder> orderList, string path) {


            try
            {
                tBestellungen orders = new tBestellungen(orderList);
                XmlSerializer x = new XmlSerializer(typeof(tBestellungen));

                var mem = new MemoryStream();
                x.Serialize(mem, orders);
                //var a = XmlSerializer.Serialize(orders,writer);


                var xmlString = System.Text.Encoding.UTF8.GetString(mem.ToArray());

                File.WriteAllText(path, xmlString);

            }
            catch (Exception e)
            {
            }
        
        
        }


        public List<string> write(List<IOrder> orderList)
        {


            try
            {
                List<string> xmlOrderList = new List<string>(); 

                foreach (var order in orderList)
                {
                    List<IOrder> singleItemOrderList = new List<IOrder>();
                    singleItemOrderList.Add(order);

                    tBestellungen orders = new tBestellungen(singleItemOrderList);
                    XmlSerializer x = new XmlSerializer(typeof(tBestellungen));

                    var mem = new MemoryStream();
                    x.Serialize(mem, orders);
                    //var a = XmlSerializer.Serialize(orders,writer);


                    var xmlString = System.Text.Encoding.UTF8.GetString(mem.ToArray());
                    xmlOrderList.Add(xmlString); 
                }

                return xmlOrderList; 

            }
            catch (Exception e)
            {
            }

            return new List<string>(); 
        }
    }
}
