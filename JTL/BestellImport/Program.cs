﻿using BestellImport.DnOrderFormat;
using BestellImport.DnOrderFormat.PlatformSpecific;
using BestellImport.DnOrderFormat.PlatformSpecific.Amazon;
using BestellImport.DnOrderFormat.PlatformSpecific.Ebay;
using BestellImport.JTL_Import;
using DN_Classes.QueriesCommands;
using DN_Classes.QueriesCommands.Orders;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml.Serialization;

namespace BestellImport
{
    class Program
    {

        public static AgentMapping agentMapping = new AgentMapping(); 
        public static Dictionary<string, SQLServer> jtlServers = new Dictionary<string, SQLServer>();


        public static string jtlVersion = "jtlold";// jtlold, jtlnew

        Program()
        {
 
        }

        static void Main(string[] args)
        {


            SQLServer oldServer = new SQLServer(@"136.243.19.216\JTLWAWI", "sa", "sa04jT14", "eazybusiness");
            SQLServer newServer = new SQLServer("144.76.166.207", "sa", "sa04jT14", "eazybusiness");
            jtlServers.Add("jtlold", oldServer);
            jtlServers.Add("jtlnew", newServer);



            SQLServer usedServer = jtlServers[jtlVersion];
            var table = usedServer.SelectCommand("select * from tXMLBestellImport");

            if (table.Rows.Count > 0)
            {
                Console.WriteLine("Table not yet empty, Run worker first");
                Thread.Sleep(TimeSpan.FromSeconds(14));
            }
            else if (table.Rows.Count == 0)
            {
                OrdersQueries orderQueries = new OrdersQueries();
                // Ebay ebay = new Ebay(orderQueries.GetEbayOrders(jtlVersion));
                // ebay.removeImported(jtlVersion, jtlServers[jtlVersion]);
                //  ebay.CreateXML();






                Amazon amazon = new Amazon(orderQueries.GetAmazonOrders(jtlVersion), DateTime.Now.AddDays(-365));
                amazon.removeImported(jtlVersion, jtlServers[jtlVersion]);

                //amazon.CreateXML();
                List<string> xmlOrderList = amazon.CreateXmlList();


                FillJtl(xmlOrderList);
            }
        }

        private static void FillJtl(List<string> xmlOrderList)
        {
            //SQLServer server = jtlServers["jtlnew"];
            SQLServer server = jtlServers["jtlold"];

            foreach (var order in xmlOrderList)
            {
                try
                {

                    server.ExecuteCommand("INSERT INTO tXMLBestellImport (cText,nPlattform,nRechnung) VALUES (@M,51,1)", order);
                }
                catch(Exception e)
                {
                    Console.WriteLine("error writing order to collection");
                }
            }
            
        }
    }
}
