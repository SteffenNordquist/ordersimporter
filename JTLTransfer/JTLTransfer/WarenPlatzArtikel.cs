﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTLTransfer
{
    class WarenPlatzArtikel
    {
         public int kWarenLagerPlatz { get; set; }
         public int kArtikel { get; set; }
         public string cKommentar_1 { get; set; }
         public string cKommentar_2 { get; set; }

        public WarenPlatzArtikel(DataRow row)
        {
            try
            {
                kWarenLagerPlatz = Convert.ToInt32(row[0]);
            }
            catch
            {
            }
            try
            {
                kArtikel = Convert.ToInt32(row[1]);
            }
            catch
            {
            }
        }
    }
}
