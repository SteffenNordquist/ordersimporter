﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTLTransfer
{
    class WarenLagetArtikelOptionen
    {
        public int kArtikel { get; set; }
        public int kWarenlager { get; set; }
        
        public int kWarenLagerPlatz { get; set; }
        public int kWMSLagerBereich { get; set; }

        public WarenLagetArtikelOptionen(DataRow row)
        {
            try
            {
                kArtikel = Convert.ToInt32(row[0]);
            }
            catch
            {
            }
            try
            {
                kWarenlager = Convert.ToInt32(row[1]);
            }
            catch
            {
                kWarenlager = 0;
            }
            try
            {
                kWarenLagerPlatz = Convert.ToInt32(row[2]);
            }
            catch
            {
                kWarenLagerPlatz = 0;
            }
            try
            {
                kWMSLagerBereich = Convert.ToInt32(row[3]);
            }
            catch
            {
                kWMSLagerBereich = 0;
            }
        }
    }
}
