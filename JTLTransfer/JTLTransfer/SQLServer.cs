﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTLTransfer
{
    public class SQLServer
    {

        private string DatabaseServer;
        private string DatabaseUsername;
        private string DatabasePassword;
        private string DatabaseName;

        public SQLServer(string databaseServer, string databaseUsername, string databasePassword, string databaseName)
        {
            this.DatabaseName = databaseName;
            this.DatabasePassword = databasePassword;
            this.DatabaseServer = databaseServer;
            this.DatabaseUsername = databaseUsername;
        }

        public DataTable SelectCommand(string SQL)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = "Data Source=" + DatabaseServer + "; Initial Catalog=" + DatabaseName + ";User ID=" + DatabaseUsername + ";Password=" + DatabasePassword;
            SqlCommand myCommand = new SqlCommand();
            SqlDataAdapter myAdapter = new SqlDataAdapter();
            DataTable myData = new DataTable();
            myCommand.Connection = conn;
            myCommand.CommandText = SQL;
            myAdapter.SelectCommand = myCommand;
            myAdapter.Fill(myData);
            conn.Close();
            conn.Dispose();
            myAdapter.Dispose();
            myCommand.Dispose();
            return myData;
        }



        public DataTable Insert(string SQL)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = "Data Source=" + DatabaseServer + "; Initial Catalog=" + DatabaseName + ";User ID=" + DatabaseUsername + ";Password=" + DatabasePassword;
            SqlCommand myCommand = new SqlCommand();
            SqlDataAdapter myAdapter = new SqlDataAdapter();
            DataTable myData = new DataTable();
            myCommand.Connection = conn;
            myCommand.CommandText = SQL;
            myAdapter.SelectCommand = myCommand;
            myAdapter.Fill(myData);
            conn.Close();
            conn.Dispose();
            myAdapter.Dispose();
            myCommand.Dispose();
            return myData;
        }


        //the executeScalar function differes from the ExecuteSelect in the sense that 
        //executeScalar only returns a single value. This is recommended when you are expecting only
        //one value from an SQL query (such as MAX,MIN, etc)
        public string ExecuteScalar(string SQL)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = "Data Source=" + DatabaseServer + "; Initial Catalog=" + DatabaseName + ";User ID=" + DatabaseUsername + ";Password=" + DatabasePassword;
            SqlCommand myCommand = new SqlCommand();
            SqlDataAdapter myAdapter = new SqlDataAdapter();
            string functionReturnValue = null;
            DataTable myData = new DataTable();
            myCommand.Connection = conn;
            myCommand.CommandText = SQL;
            conn.Open();
            try
            {
                functionReturnValue = myCommand.ExecuteScalar().ToString();
            }
            catch
            {
                functionReturnValue = "";
            }
            conn.Close();
            conn.Dispose();
            myCommand.Dispose();
            return functionReturnValue;
        }

        //executeNonQuery is used in inserting, editing, and deleting a row/s in a database
        public void ExecuteInsertUpdateDelete(string SQL)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = "Data Source=" + DatabaseServer + "; Initial Catalog=" + DatabaseName + ";User ID=" + DatabaseUsername + ";Password=" + DatabasePassword;
            SqlCommand myCommand = new SqlCommand();
            myCommand.Connection = conn;
            myCommand.CommandText = SQL;
            conn.Open();
            try
            {
                myCommand.ExecuteNonQuery();
                Console.WriteLine(" inserted!");
            
            }
            catch(Exception ex)
            {
               File.WriteAllText(@"C:\tempTools\JTLTransfer\logs\"+DateTime.Now.Ticks.ToString() +".txt", SQL+ "\nStackTrace:"+ex.StackTrace +"\nInnerException:"+ ex.InnerException);
            }
            conn.Close();
            conn.Dispose();
            myCommand.Dispose();
        }

    }
}
