﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTLTransfer
{
    class TarTikel
    {
        public int kArtikel { get; set; }
        public string cArtNr { get; set; }
        public double fVKNetto { get; set; }
        public double fUVP { get; set; }
        public string cAnmerkung { get; set; }
        public string cPreisliste { get; set; }
        public string cAktiv { get; set; }
        public double nLagerbestand { get; set; }
        public double nMindestbestellmaenge { get; set; }
        public string cBarcode { get; set; }
        public string cErloeskonto { get; set; }
        public string cTopArtikel { get; set; }
        public string cInet { get; set; }
        public string cDelInet { get; set; }
        public double fGewicht { get; set; }
        public string cNeu { get; set; }
        public string cLagerArtikel { get; set; }
        public string cTeilbar { get; set; }
        public string cLagerAktiv { get; set; }
        public string cLagerKleinerNull { get; set; }
        public double nMidestbestand { get; set; }
        public double fEKNetto { get; set; }
        public double fEbayPreis { get; set; }
        public string cLagerVariation { get; set; }
        public int nDelete { get; set; }
        public DateTime dMod { get; set; }
        public double fPackeinheit { get; set; }
        public int nVPE { get; set; }
        public double fVPEWert { get; set; }
        public string cSuchbegriffe { get; set; }
        public string cTaric { get; set; }
        public string cHerkunftsland { get; set; }
        public string kSteuerklasse { get; set; }
        public double dErstelldatum { get; set; }
        public double dErscheinungsdatum { get; set; }
        public int nSort { get; set; }
        public int kVersandklasse { get; set; }
        public double fArtGewicht { get; set; }
        public string cHAN { get; set; }
        public string cSerie { get; set; }
        public string cISBN { get; set; }
        public string cUNNummer { get; set; }
        public string cGefahrnr { get; set; }
        public string cASIN { get; set; }
        public int kEigenschaftKombi { get; set; }
        public int kVaterArtikel { get; set; }
        public int nIstVater { get; set; }
        public int nIstMindestbestand { get; set; }
        public double fAbnahmeintervall { get; set; }
        public int kStueckliste { get; set; }
        public string cUPC { get; set; }
        public int kWarengruppe { get; set; }
        public string cEPID { get; set; }
        public int nMHD { get; set; }
        public int nCharge { get; set; }
        public int nNichtBestellbar { get; set; }
        public double fAmazonVK { get; set; }
        public int nPufferTyp { get; set; }
        public int nPuffer { get; set; }
        public int nProzentualePreisStaffelAktiv { get; set; }

        public TarTikel(DataRow result)
        {
            try
            {
                kArtikel = Convert.ToInt32(result[0].ToString());
            }
            catch
            {
            }
            try
            {
            }
            catch
            {
            }
            
            try
            {
                fVKNetto = Convert.ToDouble(result[2].ToString());
            }
            catch
            {
                fVKNetto = 0;
            }

            try
            {
                fUVP = Convert.ToDouble(result[3].ToString());
            }
            catch
            {
                fUVP = 0;
            }
            cAnmerkung = result[4].ToString();
            cPreisliste = result[5].ToString();
            cAktiv = result[6].ToString();
            try
            {
                nLagerbestand = Convert.ToDouble(result[7].ToString());
            }
            catch
            {
                nLagerbestand = 0;
            }
            try
            {
                nMindestbestellmaenge = Convert.ToDouble(result[8].ToString());
            }
            catch
            {
                nMindestbestellmaenge = 0;
            }
            cBarcode = result[9].ToString();
            cErloeskonto = result[10].ToString();
            cTopArtikel = result[11].ToString();
            cInet = result[12].ToString();
            
            
            cDelInet = result[13].ToString();
            try
            {
                fGewicht = Convert.ToDouble(result[14].ToString());
            }
            catch
            {
                fGewicht = 0;

            }
            cNeu = result[15].ToString();
            cLagerArtikel = result[16].ToString();
            cTeilbar = result[17].ToString();
            cLagerAktiv = result[18].ToString();
            cLagerKleinerNull = result[19].ToString();
            try
            {
                nMidestbestand = Convert.ToDouble(result[20].ToString());
            }
            catch
            {
                nMidestbestand = 0;
            }
            try
            {
                fEKNetto = Convert.ToDouble(result[21].ToString());
            }
            catch
            {
                fEKNetto = 0;
            }
            try
            {
                fEbayPreis = Convert.ToDouble(result[22].ToString());
            }
            catch
            {
                fEbayPreis = 0;
            } 
            cLagerVariation = result[23].ToString();
            try
            {
                nDelete = Convert.ToInt32(result[24].ToString());
            }
            catch
            {
                nDelete = 0;
            } 
            try
            {
                dMod = Convert.ToDateTime(result[25].ToString());
            }
            catch
            {
                dMod = DateTime.MinValue;
            } 
            try
            {
                fPackeinheit = Convert.ToDouble(result[26].ToString());
            }
            catch
            {
                fPackeinheit = 0;
            } 
            try
            {
                nVPE = Convert.ToInt32(result[27].ToString());
            }
            catch
            {
                nVPE = 0;
            } 
            try
            {
                fVPEWert = Convert.ToDouble(result[28].ToString());
            }
            catch
            {
                fVPEWert = 0;
            } 
            cSuchbegriffe = result[29].ToString();
            cTaric = result[30].ToString();
            cHerkunftsland = result[31].ToString();
            kSteuerklasse = result[32].ToString();
            try
            {
                dErstelldatum = Convert.ToDouble(result[33].ToString());
            }
            catch
            {
                dErstelldatum = 0;
            } 
            try
            {
                dErscheinungsdatum = Convert.ToDouble(result[34].ToString());
            }
            catch
            {
                dErscheinungsdatum = 0;
            } 
            try
            {
                nSort = Convert.ToInt32(result[35].ToString());
            }
            catch
            {
                nSort = 0;
            } 
            try
            {
                kVersandklasse = Convert.ToInt32(result[36].ToString());
            }
            catch
            {
                kVersandklasse = 0;
            } 
            try
            {
                fArtGewicht = Convert.ToDouble(result[37].ToString());
            }
            catch
            {
                fArtGewicht = 0;
            } 
            cHAN = result[38].ToString();
            cSerie = result[39].ToString();
            cISBN = result[40].ToString();
            cUNNummer = result[41].ToString();
            cGefahrnr = result[42].ToString();
            cASIN = result[43].ToString();
            try
            {
                kEigenschaftKombi = Convert.ToInt32(result[44].ToString());
            }
            catch
            {
                kEigenschaftKombi = 0;
            } 
            try
            {
                kVaterArtikel = Convert.ToInt32(result[45].ToString());
            }
            catch
            {
                kVaterArtikel = 0;
            }
            try
            {
                nIstVater = Convert.ToInt32(result[46].ToString());
            }
            catch
            {
                nIstVater = 0;
            } 
            try
            {
                nIstMindestbestand = Convert.ToInt32(result[47].ToString());
            }
            catch
            {
                nIstMindestbestand = 0;
            } 
            try
            {
                nNichtBestellbar = Convert.ToInt32(result[48].ToString());
            }
            catch
            {
                nNichtBestellbar = 0;
            } 
            try
            {
                kStueckliste = Convert.ToInt32(result[49].ToString());
            }
            catch
            {
                kStueckliste = 0;
            } 
            cUPC = result[50].ToString();
            try
            {
                kWarengruppe = Convert.ToInt32(result[51].ToString());
            }
            catch
            {
                kWarengruppe = 0;
            } 
            cEPID = result[52].ToString();
            try
            {
                nMHD = Convert.ToInt32(result[53].ToString());
            }
            catch
            {
                nMHD = 0;
            } 
            try
            {
                nCharge = Convert.ToInt32(result[54].ToString());
            }
            catch
            {
                nCharge = 0;
            } 
            try
            {
                nNichtBestellbar = Convert.ToInt32(result[55].ToString());
            }
            catch
            {
                nNichtBestellbar = 0;
            } 
            try
            {
                fAmazonVK = Convert.ToDouble(result[56].ToString());
            }
            catch
            {
                fAmazonVK = 0;
            } 
            try
            {nPufferTyp = Convert.ToInt32(result[57].ToString());
            }
            catch
            {
                nPufferTyp = 0;
            } 
            try
            {nPuffer = Convert.ToInt32(result[58].ToString());
            }
            catch
            {
                nPuffer = 0;
            } 
            try
            {
                nProzentualePreisStaffelAktiv = Convert.ToInt32(result[59].ToString());
            }
            catch
            {
                nProzentualePreisStaffelAktiv = 0;
            }

            if (cInet.ToUpper() != "N" || cInet.ToUpper() != "Y")
            {
                cInet = "N";
            }
            if (cLagerAktiv.ToUpper() != "N" || cLagerAktiv.ToUpper() != "Y")
                {
                    cLagerAktiv = "N";
                }
            if (cLagerArtikel.ToUpper() != "N" || cLagerArtikel.ToUpper() != "Y")
            {
                cLagerArtikel = "N";
            }
            if (cLagerKleinerNull.ToUpper() != "N" || cLagerKleinerNull.ToUpper() != "Y")
            {
                cLagerKleinerNull = "N";
            }
            if (cLagerVariation.ToUpper() != "N" || cLagerVariation.ToUpper() != "Y")
            {
                cLagerVariation = "N";
            }
            if (cNeu.ToUpper() != "N" || cNeu.ToUpper() != "Y")
            {
                cNeu = "N";
            }
            if (cPreisliste.ToUpper() != "N" || cPreisliste.ToUpper() != "Y")
            {
                cPreisliste = "N";
            }
            if (cTeilbar.ToUpper() != "N" || cTeilbar.ToUpper() != "Y")
            {
                cTeilbar = "N";
            }
            if (cTopArtikel.ToUpper() != "N" || cTopArtikel.ToUpper() != "Y")
            {
                cTopArtikel = "N";
            }
        }
    }
}
