﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace JTLTransfer
{
    class Program
    {
        static void Main(string[] args)
        {
            SQLServer Server136 = new SQLServer("136.243.60.22", "sa", "JTL2015!", "eazybusiness");
            SQLServer Server144 = new SQLServer("144.76.166.207", "sa", "sa04jT14", "eazybusiness");

            #region tArtikel
            #region tartikelselectquery
            const string tartikeselectquery = "SELECT " +
                    "dbo.tArtikel.kArtikel, " +
                    "dbo.tArtikel.cArtNr, " +
                    "dbo.tArtikel.fVKNetto, " +
                    "dbo.tArtikel.fUVP, " +
                    "dbo.tArtikel.cAnmerkung, " +
                    "dbo.tArtikel.cPreisliste, " +
                    "dbo.tArtikel.cAktiv, " +
                    "dbo.tArtikel.nLagerbestand, " +
                    "dbo.tArtikel.nMindestbestellmaenge, " +
                    "dbo.tArtikel.cBarcode, " +
                    "dbo.tArtikel.cErloeskonto, " +
                    "dbo.tArtikel.cTopArtikel, " +
                    "dbo.tArtikel.cInet, " +
                    "dbo.tArtikel.cDelInet, " +
                    "dbo.tArtikel.fGewicht, " +
                    "dbo.tArtikel.cNeu, " +
                    "dbo.tArtikel.cLagerArtikel, " +
                    "dbo.tArtikel.cTeilbar, " +
                    "dbo.tArtikel.cLagerAktiv, " +
                    "dbo.tArtikel.cLagerKleinerNull, " +
                    "dbo.tArtikel.nMidestbestand, " +
                    "dbo.tArtikel.fEKNetto, " +
                    "dbo.tArtikel.fEbayPreis, " +
                    "dbo.tArtikel.cLagerVariation, " +
                    "dbo.tArtikel.nDelete, " +
                    "dbo.tArtikel.dMod, " +
                    "dbo.tArtikel.fPackeinheit, " +
                    "dbo.tArtikel.nVPE, " +
                    "dbo.tArtikel.fVPEWert, " +
                    "dbo.tArtikel.cSuchbegriffe, " +
                    "dbo.tArtikel.cTaric, " +
                    "dbo.tArtikel.cHerkunftsland, " +
                    "dbo.tArtikel.kSteuerklasse, " +
                    "dbo.tArtikel.dErstelldatum, " +
                    "dbo.tArtikel.dErscheinungsdatum, " +
                    "dbo.tArtikel.nSort, " +
                    "dbo.tArtikel.kVersandklasse, " +
                    "dbo.tArtikel.fArtGewicht, " +
                    "dbo.tArtikel.cHAN, " +
                    "dbo.tArtikel.cSerie, " +
                    "dbo.tArtikel.cISBN, " +
                    "dbo.tArtikel.cUNNummer, " +
                    "dbo.tArtikel.cGefahrnr, " +
                    "dbo.tArtikel.cASIN, " +
                    "dbo.tArtikel.kEigenschaftKombi, " +
                    "dbo.tArtikel.kVaterArtikel, " +
                    "dbo.tArtikel.nIstVater, " +
                    "dbo.tArtikel.nIstMindestbestand, " +
                    "dbo.tArtikel.fAbnahmeintervall, " +
                    "dbo.tArtikel.kStueckliste, " +
                    "dbo.tArtikel.cUPC, " +
                    "dbo.tArtikel.kWarengruppe, " +
                    "dbo.tArtikel.cEPID, " +
                    "dbo.tArtikel.nMHD, " +
                    "dbo.tArtikel.nCharge, " +
                    "dbo.tArtikel.nNichtBestellbar, " +
                    "dbo.tArtikel.fAmazonVK, " +
                    "dbo.tArtikel.nPufferTyp, " +
                    "dbo.tArtikel.nPuffer, " +
                    "dbo.tArtikel.nProzentualePreisStaffelAktiv " +
                    "FROM " +
                    "dbo.tlagerbestand " +
                    "INNER JOIN dbo.tartikel ON dbo.tartikel.kArtikel = dbo.tlagerbestand.kArtikel " +
                    "WHERE " +
                    "dbo.tlagerbestand.fLagerbestand > 0 ";

            #endregion
            List<string> tArtikelList = new List<string>();
            DataTable queryResult = Server136.SelectCommand(tartikeselectquery);
            foreach (DataRow result in queryResult.Rows)
            {
                #region tArtikelInsertQuery
                TarTikel tArtikel = new TarTikel(result);
                string insertQuery = "SET IDENTITY_INSERT dbo.tArtikel ON;" +
                                        "INSERT INTO dbo.tArtikel( " +
                                       "dbo.tArtikel.kArtikel, " +
                                       "dbo.tArtikel.cArtNr, " +
                                       "dbo.tArtikel.fVKNetto, " +
                                       "dbo.tArtikel.fUVP, " +
                                       "dbo.tArtikel.cAnmerkung, " +
                                       "dbo.tArtikel.cPreisliste, " +
                                       "dbo.tArtikel.cAktiv, " +
                                       "dbo.tArtikel.nLagerbestand, " +
                                       "dbo.tArtikel.nMindestbestellmaenge, " +
                                       "dbo.tArtikel.cBarcode, " +
                                       "dbo.tArtikel.cErloeskonto, " +
                                       "dbo.tArtikel.cTopArtikel, " +
                                       "dbo.tArtikel.cInet, " +
                                       "dbo.tArtikel.cDelInet, " +
                                       "dbo.tArtikel.fGewicht, " +
                                       "dbo.tArtikel.cNeu, " +
                                       "dbo.tArtikel.cLagerArtikel, " +
                                       "dbo.tArtikel.cTeilbar, " +
                                       "dbo.tArtikel.cLagerAktiv, " +
                                       "dbo.tArtikel.cLagerKleinerNull, " +
                                       "dbo.tArtikel.nMidestbestand, " +
                                       "dbo.tArtikel.fEKNetto, " +
                                       "dbo.tArtikel.fEbayPreis, " +
                                       "dbo.tArtikel.cLagerVariation, " +
                                       "dbo.tArtikel.nDelete, " +
                                       "dbo.tArtikel.dMod, " +
                                       "dbo.tArtikel.fPackeinheit, " +
                                       "dbo.tArtikel.nVPE, " +
                                       "dbo.tArtikel.fVPEWert, " +
                                       "dbo.tArtikel.cSuchbegriffe, " +
                                       "dbo.tArtikel.cTaric, " +
                                       "dbo.tArtikel.cHerkunftsland, " +
                                       "dbo.tArtikel.kSteuerklasse, " +
                                       "dbo.tArtikel.dErstelldatum, " +
                                       "dbo.tArtikel.dErscheinungsdatum, " +
                                       "dbo.tArtikel.nSort, " +
                                       "dbo.tArtikel.kVersandklasse, " +
                                       "dbo.tArtikel.fArtGewicht, " +
                                       "dbo.tArtikel.cHAN, " +
                                       "dbo.tArtikel.cSerie, " +
                                       "dbo.tArtikel.cISBN, " +
                                       "dbo.tArtikel.cUNNummer, " +
                                       "dbo.tArtikel.cGefahrnr, " +
                                       "dbo.tArtikel.cASIN, " +
                                       "dbo.tArtikel.kEigenschaftKombi, " +
                                       "dbo.tArtikel.kVaterArtikel, " +
                                       "dbo.tArtikel.nIstVater, " +
                                       "dbo.tArtikel.nIstMindestbestand, " +
                                       "dbo.tArtikel.fAbnahmeintervall, " +
                                       "dbo.tArtikel.kStueckliste, " +
                                       "dbo.tArtikel.cUPC, " +
                                       "dbo.tArtikel.kWarengruppe, " +
                                       "dbo.tArtikel.cEPID, " +
                                       "dbo.tArtikel.nMHD, " +
                                       "dbo.tArtikel.nCharge, " +
                                       "dbo.tArtikel.nNichtBestellbar, " +
                                       "dbo.tArtikel.fAmazonVK, " +
                                       "dbo.tArtikel.nPufferTyp, " +
                                       "dbo.tArtikel.nPuffer, " +
                                       "dbo.tArtikel.nProzentualePreisStaffelAktiv " +
                                       ") VALUES ( " +
                                       tArtikel.kArtikel + "," +
                                      "'" + tArtikel.cArtNr + "'," +
                                       tArtikel.fVKNetto + "," +
                                       tArtikel.fUVP + "," +
                                      "'" + tArtikel.cAnmerkung + "'," +
                                     "'" + tArtikel.cPreisliste + "'," +
                                      "'" + tArtikel.cAktiv + "'," +
                                       tArtikel.nLagerbestand + "," +
                                       tArtikel.nMindestbestellmaenge + "," +
                                     "'" + tArtikel.cBarcode + "'," +
                                     "'" + tArtikel.cErloeskonto + "'," +
                                    "'" + tArtikel.cTopArtikel + "'," +
                                    "'" + tArtikel.cInet + "'," +
                                    "'" + tArtikel.cDelInet + "'," +
                                       tArtikel.fGewicht + "," +
                                    "'" + tArtikel.cNeu + "'," +
                                   "'" + tArtikel.cLagerArtikel + "'," +
                                   "'" + tArtikel.cTeilbar + "'," +
                                   "'" + tArtikel.cLagerAktiv + "'," +
                                   "'" + tArtikel.cLagerKleinerNull + "'," +
                                       tArtikel.nMidestbestand + "," +
                                       tArtikel.fEKNetto + "," +
                                       tArtikel.fEbayPreis + "," +
                                    "'" + tArtikel.cLagerVariation + "'," +
                                       tArtikel.nDelete + "," +
                                     "'" + tArtikel.dMod.ToString("yyyy-MM-dd HH:mm:ss.fff") + "'," +
                                       tArtikel.fPackeinheit + "," +
                                       tArtikel.nVPE + "," +
                                       tArtikel.fVPEWert + "," +
                                    "'" + tArtikel.cSuchbegriffe + "'," +
                                    "'" + tArtikel.cTaric + "'," +
                                     "'" + tArtikel.cHerkunftsland + "'," +
                                     "'" + tArtikel.kSteuerklasse + "'," +
                                       tArtikel.dErstelldatum + "," +
                                       tArtikel.dErscheinungsdatum + "," +
                                       tArtikel.nSort + "," +
                                       tArtikel.kVersandklasse + "," +
                                       tArtikel.fArtGewicht + "," +
                                     "'" + tArtikel.cHAN + "'," +
                                     "'" + tArtikel.cSerie + "'," +
                                     "'" + tArtikel.cISBN + "'," +
                                     "'" + tArtikel.cUNNummer + "'," +
                                     "'" + tArtikel.cGefahrnr + "'," +
                                     "'" + tArtikel.cASIN + "'," +
                                       tArtikel.kEigenschaftKombi + "," +
                                       tArtikel.kVaterArtikel + "," +
                                       tArtikel.nIstVater + "," +
                                       tArtikel.nIstMindestbestand + "," +
                                       tArtikel.fAbnahmeintervall + "," +
                                       tArtikel.kStueckliste + "," +
                                       "'" + tArtikel.cUPC + "'," +
                                       tArtikel.kWarengruppe + "," +
                                       "'" + tArtikel.cEPID + "'," +
                                       tArtikel.nMHD + "," +
                                       tArtikel.nCharge + "," +
                                       tArtikel.nNichtBestellbar + "," +
                                       tArtikel.fAmazonVK + "," +
                                       tArtikel.nPufferTyp + "," +
                                       tArtikel.nPuffer + "," +
                                       tArtikel.nProzentualePreisStaffelAktiv + ");SET IDENTITY_INSERT dbo.tArtikel OFF;";
                #endregion
                tArtikelList.Add(result[0].ToString());
                int count =
                    Convert.ToInt32(Server144.ExecuteScalar("SELECT COUNT(*) FROM dbo.tArtikel WHERE dbo.tArtikel.kArtikel='" + result[0] + "';"));
                if (count <= 0)
                {
                    Console.Write("tArtikel: " + result[0]);
                    Server144.ExecuteInsertUpdateDelete(insertQuery);
                }
            }
            File.WriteAllLines("C:\\tempTools\\tArtikel.txt", tArtikelList);

            //add the quantities in the 144 database.
            foreach (var item in tArtikelList)
            {
                #region LagerBestand
                foreach (DataRow row in Server136.SelectCommand("SELECT " +
                                                                "dbo.tlagerbestand.kArtikel," +
                                                                "dbo.tlagerbestand.fLagerbestand," +
                                                                "dbo.tlagerbestand.fVerfuegbar," +
                                                                "dbo.tlagerbestand.fVerfuegbarGesperrt," +
                                                                "dbo.tlagerbestand.fZulauf," +
                                                                "dbo.tlagerbestand.fAufEinkaufsliste," +
                                                                "dbo.tlagerbestand.dLieferdatum FROM dbo.tlagerbestand " +
                                                                "WHERE dbo.tlagerbestand.kArtikel=" + item + ";").Rows)
                {
                    LagerBestand lbt = new LagerBestand(row);
                    int count =
                        Convert.ToInt32(
                            Server144.ExecuteScalar(
                                "SELECT COUNT(*) FROM dbo.tlagerbestand WHERE dbo.tlagerbestand.kArtikel=" + item));
                    Console.WriteLine(item + "\t" + count);
                    if (count > 0)
                    {
                        Server144.ExecuteInsertUpdateDelete("UPDATE dbo.tlagerbestand SET " +
                                                            "dbo.tlagerbestand.fLagerbestand=" + lbt.fLagerbestand + "," +
                                                            "dbo.tlagerbestand.fVerfuegbar=" + lbt.fVerfuegbar + "," +
                                                            "dbo.tlagerbestand.fVerfuegbarGesperrt=" +
                                                            lbt.fVerfuegbarGesperrt + "," +
                                                            "dbo.tlagerbestand.fZulauf=" + lbt.fZulauf + "," +
                                                            "dbo.tlagerbestand.fAufEinkaufsliste=" +
                                                            lbt.fAufEinkaufsliste + "," +
                                                            "dbo.tlagerbestand.dLieferdatum=" + lbt.dLieferdatum +
                                                            " WHERE dbo.tlagerbestand.kArtikel=" + lbt.kArtikel);
                    }
                    else
                    {
                        Server144.ExecuteInsertUpdateDelete("INSERT INTO dbo.tlagerbestand(" +
                                                                "dbo.tlagerbestand.kArtikel," +
                                                                "dbo.tlagerbestand.fLagerbestand," +
                                                                "dbo.tlagerbestand.fVerfuegbar," +
                                                                "dbo.tlagerbestand.fVerfuegbarGesperrt," +
                                                                "dbo.tlagerbestand.fZulauf," +
                                                                "dbo.tlagerbestand.fAufEinkaufsliste," +
                                                                "dbo.tlagerbestand.dLieferdatum) VALUES (" +
                                                                lbt.kArtikel + "," +
                                                                lbt.fLagerbestand + "," +
                                                                lbt.fVerfuegbar + "," +
                                                                lbt.fVerfuegbarGesperrt + "," +
                                                                lbt.fZulauf + "," +
                                                                lbt.fAufEinkaufsliste + "," +
                                                                lbt.dLieferdatum + ");");
                    }
                }
                #endregion
            }
            #endregion

            #region tWarenLagerPlatzArtikel
            #region tWarenLagerPlatzArtikelselectquery
            const string tWarenLagerPlatzArtikelselectquery = "SELECT " +
                                                              "dbo.tWarenLagerPlatzArtikel.kWarenLagerPlatz, " +
                                                              "dbo.tWarenLagerPlatzArtikel.kArtikel, " +
                                                              "dbo.tWarenLagerPlatzArtikel.cKommentar_1, " +
                                                              "dbo.tWarenLagerPlatzArtikel.cKommentar_2 " +
                                                              "FROM " +
                                                              "dbo.tWarenLagerPlatzArtikel";
            #endregion
            List<string> tWarenLagerPlatzArtikelList = new List<string>();
            DataTable querytWarenLagerPlatzArtikelselectqueryResult = Server136.SelectCommand(tWarenLagerPlatzArtikelselectquery);
            foreach (DataRow result in querytWarenLagerPlatzArtikelselectqueryResult.Rows)
            {
                #region tWarenLagerPlatzArtikelInsertQuery

                WarenPlatzArtikel wpa = new WarenPlatzArtikel(result);
                string insertQuery = "INSERT INTO dbo.tWarenLagerPlatzArtikel( " +
                                     "dbo.tWarenLagerPlatzArtikel.kWarenLagerPlatz, " +
                                     "dbo.tWarenLagerPlatzArtikel.kArtikel, " +
                                     "dbo.tWarenLagerPlatzArtikel.cKommentar_1, " +
                                     "dbo.tWarenLagerPlatzArtikel.cKommentar_2 " +
                                     ") VALUES ( " +
                                     wpa.kWarenLagerPlatz + "," +
                                     wpa.kArtikel + "," +
                                     "'" + wpa.cKommentar_1 + "'," +
                                     "'" + wpa.cKommentar_2 + "');";

                #endregion

                int count = 0;
                try
                {
                    count = Convert.ToInt32(Server144.ExecuteScalar("SELECT COUNT(*) FROM dbo.tWarenLagerPlatzArtikel WHERE " +
                                                            "dbo.tWarenLagerPlatzArtikel.kWarenLagerPlatz='" + result[0] +
                                                            "and dbo.tWarenLagerPlatzArtikel.kArtikel='" + result[1] +

                                            "';"));
                }
                catch
                {
                }
                Console.WriteLine("SELECT COUNT(*) FROM dbo.tWarenLagerPlatzArtikel WHERE " +
                                                            "dbo.tWarenLagerPlatzArtikel.kWarenLagerPlatz='" + result[0] +
                                                            "and dbo.tWarenLagerPlatzArtikel.kArtikel='" + result[1] +

                                            "';");
                Console.Write(count);
                if (count <= 0)
                {
                    Console.WriteLine("tWarenLagerPlatzArtikel: " + result[0] + " inserted;");
                    tWarenLagerPlatzArtikelList.Add(result[0] + " inserted;");
                    Server144.ExecuteInsertUpdateDelete(insertQuery);
                }
            }
            File.WriteAllLines("C:\\tempTools\\tWarenLagerPlatzArtikel.txt", tWarenLagerPlatzArtikelList);

            #endregion

            /*
            #region tWarenLagerAusgang
            #region tWarenLagerAusgangselectquery
            const string tWarenLagerAusgangselectquery = "SELECT " +
                                                    "dbo.tWarenLagerAusgang.kWarenLagerEingang, " +
                                                    "dbo.tWarenLagerAusgang.kLieferscheinPos, " +
                                                    "dbo.tWarenLagerAusgang.fAnzahl, " +
                                                    "dbo.tWarenLagerAusgang.kWarenLagerAusgang, " +
                                                    "dbo.tWarenLagerAusgang.kWarenLagerPlatz, " +
                                                    "dbo.tWarenLagerAusgang.kArtikel, " +
                                                    "dbo.tWarenLagerAusgang.cKommentar, " +
                                                    "dbo.tWarenLagerAusgang.dErstellt, " +
                                                    "dbo.tWarenLagerAusgang.kBenutzer " +
                                                    "FROM " +
                                                    "dbo.tWarenLagerAusgang; ";

            #endregion
            List<string> tWarenLagerAusgangList = new List<string>();
            DataTable querytWarenLagerAusgangselectqueryResult = Server136.SelectCommand(tWarenLagerAusgangselectquery);
            foreach (DataRow result in querytWarenLagerAusgangselectqueryResult.Rows)
            {
                #region tWarenLagerAusgangListInsertQuery
                string insertQuery = "SET IDENTITY_INSERT dbo.tWarenLagerAusgang ON;" +
                                     "INSERT INTO dbo.tWarenLagerAusgang( " +
                                   "dbo.tWarenLagerAusgang.kWarenLagerEingang, " +
                                    "dbo.tWarenLagerAusgang.kLieferscheinPos, " +
                                    "dbo.tWarenLagerAusgang.fAnzahl, " +
                                    "dbo.tWarenLagerAusgang.kWarenLagerAusgang, " +
                                    "dbo.tWarenLagerAusgang.kWarenLagerPlatz, " +
                                    "dbo.tWarenLagerAusgang.kArtikel, " +
                                    "dbo.tWarenLagerAusgang.cKommentar, " +
                                    "dbo.tWarenLagerAusgang.dErstellt, " +
                                    "dbo.tWarenLagerAusgang.kBenutzer " +
                                     ") VALUES ( '" +
                                    result[0] + "','" +
                                    result[1] + "','" +
                                    result[2] + "','" +
                                    result[3] + "','" +
                                    result[4] + "','" +
                                    result[5] + "','" +
                                    result[6] + "','" +
                                    result[7] + "','" +
                                    result[8] + "');" +
                                     "SET IDENTITY_INSERT dbo.tWarenLagerAusgang OFF;";
                #endregion
                int count =
                    Convert.ToInt32(Server144.ExecuteScalar("SELECT COUNT(*) FROM dbo.tWarenLagerAusgang WHERE dbo.tWarenLagerAusgang.kWarenLagerAusgang='" + result[3] +
                                            "';"));
                if (count <= 0)
                {
                    Console.WriteLine("tWarenLagerAusgang: " + result[0] + " inserted;");
                    tWarenLagerAusgangList.Add(result[0] + " inserted;");
                    Server144.ExecuteInsertUpdateDelete(insertQuery);
                }
            }
            File.WriteAllLines("C:\\tempTools\\tWarenLagerAusgang.txt", tWarenLagerAusgangList);

            #endregion
            #region tWarenlagerArtikelOptionen
            #region tWarenlagerArtikelOptionenselectquery

            const string tWarenlagerArtikelOptionenselectquery = "SELECT " +
                                 "dbo.tWarenlagerArtikelOptionen.kArtikel, " +
                                 "dbo.tWarenlagerArtikelOptionen.kWarenlager, " +
                                 "dbo.tWarenlagerArtikelOptionen.kWarenLagerPlatz, " +
                                 "dbo.tWarenlagerArtikelOptionen.kWMSLagerBereich " +
                                 "FROM " +
                                 "dbo.tWarenlagerArtikelOptionen; ";
            #endregion
            List<string> tWarenlagerArtikelOptionenselectqueryList = new List<string>();
            DataTable querytWarenlagerArtikelOptionenselectqueryResult = Server136.SelectCommand(tWarenlagerArtikelOptionenselectquery);
            foreach (DataRow result in querytWarenlagerArtikelOptionenselectqueryResult.Rows)
            {
                WarenLagetArtikelOptionen t = new WarenLagetArtikelOptionen(result);
                #region tWarenlagerArtikelOptionenInsertQuery

                string insertQuery = "SET IDENTITY_INSERT dbo.tWarenlagerArtikelOptionen ON;" +
                                     "INSERT INTO dbo.tWarenlagerArtikelOptionen( " +
                                     "dbo.tWarenlagerArtikelOptionen.kArtikel, " +
                                     "dbo.tWarenlagerArtikelOptionen.kWarenlager, " +
                                     "dbo.tWarenlagerArtikelOptionen.kWarenLagerPlatz, " +
                                     "dbo.tWarenlagerArtikelOptionen.kWMSLagerBereich " +
                                     ") VALUES ( " +
                                   t.kArtikel + "," +
                                     t.kWarenlager + "," +
                                     t.kWarenLagerPlatz + "," +
                                     t.kWMSLagerBereich + ",);SET IDENTITY_INSERT dbo.tWarenlagerArtikelOptionen OFF;";
                #endregion
                int count =
                    Convert.ToInt32(Server144.ExecuteScalar("SELECT COUNT(*) FROM dbo.tWarenlagerArtikelOptionen WHERE dbo.tWarenlagerArtikelOptionen.kArtikel='" + result[0] +
                                            "';"));
                if (count <= 0)
                {
                    Console.WriteLine("tWarenlagerArtikelOptionen: " + result[0] + " inserted;");
                    tWarenlagerArtikelOptionenselectqueryList.Add(result[0] + " inserted;");
                    Server144.ExecuteInsertUpdateDelete(insertQuery);
                }
            }
            File.WriteAllLines("C:\\tempTools\\tWarenlagerArtikelOptionen.txt", tWarenlagerArtikelOptionenselectqueryList);

            #endregion
            #region tWarenLagerEingang
            #region tWarenLagerEingangselectquery
            const string tWarenLagerEingangselectquery = "SELECT " +
                                                         "dbo.tWarenLagerEingang.kWarenLagerEingang, " +
                                                         "dbo.tWarenLagerEingang.kArtikel, " +
                                                         "dbo.tWarenLagerEingang.kWarenLagerPlatz, " +
                                                         "dbo.tWarenLagerEingang.kLieferantenBestellungPos, " +
                                                         "dbo.tWarenLagerEingang.kBenutzer, " +
                                                         "dbo.tWarenLagerEingang.fAnzahl, " +
                                                         "dbo.tWarenLagerEingang.fEKEinzel, " +
                                                         "dbo.tWarenLagerEingang.cLieferscheinNr, " +
                                                         "dbo.tWarenLagerEingang.cChargenNr, " +
                                                         "dbo.tWarenLagerEingang.dMHD, " +
                                                         "dbo.tWarenLagerEingang.dErstellt, " +
                                                         "dbo.tWarenLagerEingang.dGeliefertAM, " +
                                                         "dbo.tWarenLagerEingang.cKommentar, " +
                                                         "dbo.tWarenLagerEingang.kGutschriftPos, " +
                                                         "dbo.tWarenLagerEingang.kWarenLagerAusgang, " +
                                                         "dbo.tWarenLagerEingang.kLHM, " +
                                                         "dbo.tWarenLagerEingang.fAnzahlAktuell, " +
                                                         "dbo.tWarenLagerEingang.fAnzahlReserviertPickpos, " +
                                                         "dbo.tWarenLagerEingang.kSessionID " +
                                                         "FROM " +
                                                         "dbo.tWarenLagerEingang ";


            #endregion
            List<string> tWarenLagerEingangList = new List<string>();
            DataTable querytWarenLagerEingangselectqueryResult = Server136.SelectCommand(tWarenLagerEingangselectquery);
            foreach (DataRow result in querytWarenLagerEingangselectqueryResult.Rows)
            {
                #region tWarenLagerAusgangListInsertQuery

                string insertQuery = "SET IDENTITY_INSERT dbo.tWarenLagerEingang ON;" +
                                     "INSERT INTO dbo.tWarenLagerEingang( " +
                                     "dbo.tWarenLagerEingang.kWarenLagerEingang, " +
                                     "dbo.tWarenLagerEingang.kArtikel, " +
                                     "dbo.tWarenLagerEingang.kWarenLagerPlatz, " +
                                     "dbo.tWarenLagerEingang.kLieferantenBestellungPos, " +
                                     "dbo.tWarenLagerEingang.kBenutzer, " +
                                     "dbo.tWarenLagerEingang.fAnzahl, " +
                                     "dbo.tWarenLagerEingang.fEKEinzel, " +
                                     "dbo.tWarenLagerEingang.cLieferscheinNr, " +
                                     "dbo.tWarenLagerEingang.cChargenNr, " +
                                     "dbo.tWarenLagerEingang.dMHD, " +
                                     "dbo.tWarenLagerEingang.dErstellt, " +
                                     "dbo.tWarenLagerEingang.dGeliefertAM, " +
                                     "dbo.tWarenLagerEingang.cKommentar, " +
                                     "dbo.tWarenLagerEingang.kGutschriftPos, " +
                                     "dbo.tWarenLagerEingang.kWarenLagerAusgang, " +
                                     "dbo.tWarenLagerEingang.kLHM, " +
                                     "dbo.tWarenLagerEingang.fAnzahlAktuell, " +
                                     "dbo.tWarenLagerEingang.fAnzahlReserviertPickpos, " +
                                     "dbo.tWarenLagerEingang.kSessionID " +
                                     ") VALUES ( '" +
                                     result[0] + "','" +
                                     result[1] + "','" +
                                     result[2] + "','" +
                                     result[3] + "','" +
                                     result[4] + "','" +
                                     result[5] + "','" +
                                     result[6] + "','" +
                                     result[7] + "','" +
                                     result[9] + "','" +
                                     result[10] + "','" +
                                     result[11] + "','" +
                                     result[12] + "','" +
                                     result[13] + "','" +
                                     result[14] + "','" +
                                     result[15] + "','" +
                                     result[16] + "','" +
                                     result[17] + "','" +
                                     result[18] + "');" +
                                     "SET IDENTITY_INSERT dbo.tWarenLagerEingang OFF;";
                #endregion
                int count =
                    Convert.ToInt32(Server144.ExecuteScalar("SELECT COUNT(*) FROM dbo.tWarenLagerEingang WHERE dbo.tWarenLagerEingang.kWarenLagerEingang='" + result[0] +
                                            "';"));
                if (count <= 0)
                {
                    Console.WriteLine("tWarenLagerEingang: " + result[0] + " inserted;");
                    tWarenLagerAusgangList.Add(result[0] + " inserted;");
                    Server144.ExecuteInsertUpdateDelete(insertQuery);
                }
            }
            File.WriteAllLines("C:\\tempTools\\tWarenLagerEingang.txt", tWarenLagerAusgangList);

            #endregion
            */

        }
    }
}
