﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTLTransfer
{
    class LagerBestand
    {
        public int kArtikel { get; set; }
        public double fLagerbestand { get; set; }
        public double fVerfuegbar { get; set; }
        public double fVerfuegbarGesperrt { get; set; }
        public double fZulauf { get; set; }
        public double fAufEinkaufsliste { get; set; }
        public double dLieferdatum { get; set; }


        public LagerBestand(DataRow row)
        {
            try
            {
                kArtikel = Convert.ToInt32(row[0]);
            }
            catch
            {
            }
            try
            {
                fLagerbestand = Convert.ToDouble(row[1]);
            }
            catch
            {
                fLagerbestand = 0;
            }
            try
            {
                fVerfuegbar = Convert.ToDouble(row[2]);
            }
            catch
            {
                fVerfuegbar = 0;
            }
           
            try
            {
                fVerfuegbarGesperrt = Convert.ToDouble(row[4]);
            }
            catch
            {
                fVerfuegbarGesperrt = 0;
            }
            try
            {
                fZulauf = Convert.ToDouble(row[5]);
            }
            catch
            {
                fZulauf = 0;
            }
            try
            {
                fAufEinkaufsliste = Convert.ToDouble(row[6]);
            }
            catch
            {
                fAufEinkaufsliste = 0;
            }
            try
            {
                dLieferdatum = Convert.ToDouble(row[7]);
            }
            catch
            {
                dLieferdatum = 0;
            }
        }
    }
}
